package contains_duplicate

import (
	"sort"
)

// O(nlogn)
// Space: O(logn)
func containsDuplicate(nums []int) bool {
	sort.Ints(nums)
	for i := 1; i < len(nums); i++ {
		if nums[i] == nums[i-1] {
			return true
		}
	}
	return false
}

// Brute Force
// Time: O(n^2)
// Space: O(1)
func containsDuplicateAlt(nums []int) bool {
	for i := 0; i < len(nums)-1; i++ {
		println(i)
		for j := i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] {
				return true
			}
		}
	}
	return false
}

// Set
// Time: O(n)
// Space: O(n)
func containsDuplicateAlt2(nums []int) bool {
	set := make(map[int]struct{})
	for _, num := range nums {
		if _, ok := set[num]; ok {
			return true
		}
		set[num] = struct{}{}
	}
	return false
}
