package contains_duplicate

import "testing"

func TestContains_Duplicate(t *testing.T) {
	testcases := []struct {
		nums     []int
		expected bool
	}{
		{[]int{1, 2, 3, 4, 5}, false},          // No duplicates
		{[]int{1, 2, 3, 4, 1}, true},           // Duplicate: 1
		{[]int{3, 4, 2, 1, 2}, true},           // Duplicate: 2
		{[]int{5, 5, 5, 5, 5}, true},           // All duplicates
		{[]int{9, 8, 7, 6, 5}, false},          // No duplicates
		{[]int{3, 6, 8, 9, 3}, true},           // Duplicate: 3
		{[]int{2, 2}, true},                    // Duplicate: 2
		{[]int{4, 7, 2, 1, 9, 3, 6, 5}, false}, // No duplicates
	}

	for _, testcase := range testcases {
		res := containsDuplicate(testcase.nums)
		if res != testcase.expected {
			t.Errorf("For input %v, expected %v, but got %v", testcase.nums, testcase.expected, res)
		}
	}

}
