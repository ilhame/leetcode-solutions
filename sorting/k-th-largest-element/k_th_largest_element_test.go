package k_th_largest_element

import "testing"

func TestFindKthLargest(t *testing.T) {
	tests := []struct {
		nums     []int
		k        int
		expected int
	}{
		{[]int{3, 2, 1, 5, 4}, 1, 5}, // Largest element is 5
		{[]int{3, 2, 1, 5, 4}, 2, 4}, // 2nd largest element is 4
		{[]int{3, 2, 1, 5, 4}, 3, 3}, // 3rd largest element is 3
		{[]int{3, 2, 1, 5, 4}, 4, 2}, // 4th largest element is 2
		{[]int{3, 2, 1, 5, 4}, 5, 1}, // 5th largest element is 1
		{[]int{5, 5, 5, 5, 5}, 1, 5}, // All elements are the same
		{[]int{1, 2, 3, 4, 5}, 5, 1}, // Smallest element is 1
		{[]int{1, 2, 3, 4, 5}, 1, 5}, // Largest element is 5
	}

	for _, test := range tests {
		result := findKthLargest(test.nums, test.k)
		if result != test.expected {
			t.Errorf("For input %v and k %d, expected %d, but got %d", test.nums, test.k, test.expected, result)
		}
	}
}
