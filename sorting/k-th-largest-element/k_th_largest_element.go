package k_th_largest_element

// Time Complexity: O(n) on average, O(n^2) in the worst case.
// Space Complexity: O(1).
func findKthLargest(nums []int, k int) int {
	n := len(nums)
	start, end := 0, n-1

	for start <= end {
		idx := partition(nums, start, end)
		if idx == n-k {
			return nums[idx]
		} else if idx < n-k {
			start = idx + 1
		} else {
			end = idx - 1
		}
	}
	return -1

}

func partition(nums []int, start, end int) int {
	pivot := (start + end) / 2
	nums[pivot], nums[end] = nums[end], nums[pivot]
	left := start
	for i := start; i <= end; i++ {
		if nums[i] < nums[end] {
			nums[i], nums[left] = nums[left], nums[i]
			left++
		}
	}
	nums[left], nums[end] = nums[end], nums[left]
	return left

}

///////////////////////////////////////////////////////////////////////////////////////
