package max_prod_of_three_numbers

import "testing"

func TestMaximumProduct(t *testing.T) {
	tests := []struct {
		nums     []int
		expected int
	}{
		{[]int{-1, -2, -3}, -6},            // Maximum product is -6
		{[]int{-1, -2, -3, 4}, 24},         // Maximum product is 24
		{[]int{1, 2, 3, 4}, 24},            // Maximum product is 24
		{[]int{-10, -10, 1, 3, 2}, 300},    // Maximum product is 300
		{[]int{-10, -10, -1, -3, -2}, -6},  // Maximum product is -6
		{[]int{-10, -9, 1, 2, 3}, 270},     // Maximum product is 270
		{[]int{-10, -9, -8, -7, -6}, -336}, // Maximum product is -336
		{[]int{1, 2, 3, 4, 5}, 60},         // Maximum product is 60
		{[]int{-1, -2, -3, -4, -5}, -6},    // Maximum product is -6
		{[]int{-1, -2, -3, -4, 0, 5}, 60},  // Maximum product is 60
	}

	for _, test := range tests {
		result := maximumProduct(test.nums)
		if result != test.expected {
			t.Errorf("For input %v, expected %v, but got %v", test.nums, test.expected, result)
		}
	}
}
