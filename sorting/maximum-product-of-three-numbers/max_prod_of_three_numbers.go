package max_prod_of_three_numbers

import (
	"math"
	"sort"
)

func main() {

}

//Constrains:
//3 <= nums.length <= 104
//-1000 <= nums[i] <= 1000

// Time :(n*log(n))
// Space o(log(n))
/*
The space complexity of the sort.Ints() function from the Go sort package is O(log n), where n is the length of the input slice. This is because the sort.Ints() function uses an adaptive version of the quicksort algorithm.
*/
func maximumProduct(nums []int) int {
	sort.Ints(nums)
	return max(nums[len(nums)-1]*nums[len(nums)-2]*nums[len(nums)-3], nums[0]*nums[1]*nums[len(nums)-1])

}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// Time: o(n)
// Space o(1)
func maximumProductAlt(nums []int) int {

	fmax, smax, tmax := math.MinInt, math.MinInt, math.MinInt

	for i := 0; i < len(nums); i++ {
		if nums[i] > fmax {
			tmax = smax
			smax = fmax
			fmax = nums[i]
		} else if nums[i] > smax {
			tmax = smax
			smax = nums[i]
		} else if nums[i] > tmax {
			tmax = nums[i]
		}
	}

	fmin, smin := math.MaxInt, math.MaxInt

	for i := 0; i < len(nums); i++ {
		if nums[i] < fmin {
			smin = fmin
			fmin = nums[i]
		} else if nums[i] < smin {
			smin = nums[i]
		}
	}

	return max(fmax*smax*tmax, fmax*fmin*smin)

}
