package valid_anagram

import (
	"sort"
)

/*Given two strings s and t, return true if t is an anagram of s, and false otherwise.
An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
s and t consist of lowercase English letters. */

// Solution with sorting
// Time: O(n * log n)
// Space: O(n)
func isAnagramAlt(s, t string) bool {
	r1 := []rune(s)
	r2 := []rune(t)
	sort.Slice(r1, func(i, j int) bool {
		return r1[i] < r1[j]
	})
	sort.Slice(r2, func(i, j int) bool {
		return r2[i] < r2[j]
	})
	return string(r1) == string(r2)
}

// Solution with slice (instead of slice [26]int array can be used)
// Time o(n)
// Space o(1)
func isAnagram(s, t string) bool {
	if len(s) != len(t) {
		return false
	}
	count := make([]int, 26)

	for i := 0; i < len(s); i++ {
		count[s[i]-'a']++
		count[t[i]-'a']--
	}

	for _, val := range count {
		if val != 0 {
			return false
		}
	}
	return true

}
