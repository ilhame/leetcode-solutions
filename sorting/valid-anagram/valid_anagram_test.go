package valid_anagram

import "testing"

func TestIsAnagram(t *testing.T) {
	tests := []struct {
		s        string
		t        string
		expected bool
	}{
		{"anagram", "nagaram", true}, // Anagrams
		{"rat", "car", false},        // Not anagrams
		{"listen", "silent", true},   // Anagrams
		{"hello", "world", false},    // Not anagrams
		{"a", "a", true},             // Anagrams (single character)
		{"", "", true},               // Anagrams (empty strings)
		{"abcdefg", "gfedcba", true}, // Anagrams
		{"test", "ttes", true},       // Anagrams
	}
	for _, test := range tests {
		result := isAnagram(test.s, test.t)
		if result != test.expected {
			t.Errorf("For inputs '%s' and '%s', expected %v, but got %v", test.s, test.t, test.expected, result)
		}
	}
}
