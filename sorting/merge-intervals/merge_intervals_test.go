package merge_intervals

import "testing"

func TestMerge(t *testing.T) {
	tests := []struct {
		intervals [][]int
		expected  [][]int
	}{
		{
			[][]int{{1, 3}, {2, 6}, {8, 10}, {15, 18}},
			[][]int{{1, 6}, {8, 10}, {15, 18}},
		},
		{
			[][]int{{1, 4}, {4, 5}},
			[][]int{{1, 5}},
		},
		{
			[][]int{{1, 4}, {2, 3}},
			[][]int{{1, 4}},
		},
		{
			[][]int{{1, 3}},
			[][]int{{1, 3}},
		},
		{
			[][]int{{1, 3}, {3, 6}, {6, 9}},
			[][]int{{1, 9}},
		},
		{
			[][]int{{1, 2}, {3, 4}, {5, 6}},
			[][]int{{1, 2}, {3, 4}, {5, 6}},
		},
	}

	for _, test := range tests {
		result := merge(test.intervals)
		if !isTwoDArrayEqual(result, test.expected) {
			t.Errorf("For input %v, expected %v, but got %v", test.intervals, test.expected, result)
		}
	}
}

func isTwoDArrayEqual(arr1, arr2 [][]int) bool {
	if len(arr1) != len(arr2) {
		return false
	}
	for i := 0; i < len(arr1); i++ {
		if !isEqual(arr1[i], arr2[i]) {
			return false
		}
	}
	return true
}

func isEqual(slice1, slice2 []int) bool {
	if len(slice1) != len(slice2) {
		return false
	}
	for i := range slice1 {
		if slice1[i] != slice2[i] {
			return false
		}
	}
	return true
}
