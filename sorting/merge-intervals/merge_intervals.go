package merge_intervals

import "sort"

/*
Sorting the intervals array based on the start times: O(n * log n), where n is the number of intervals.
The loop that processes the sorted intervals and merges overlapping ones: O(n), where n is the number of intervals.
The dominant factor here is the sorting step, which is O(n * log n) due to the sorting algorithm's complexity. Therefore, the overall time complexity of the merge function is O(n * log n).
*/
//Time : o(n*log(n))
/*
Creating the res slice to store the merged intervals: O(n), where n is the number of intervals.

Creating the cur slice to store the current interval: O(1), since it's a fixed-size slice.

Creating the sorted intervals slice: O(n), where n is the number of intervals.

Overall, the space complexity of the merge function is dominated by the res slice, which is O(n).
*/
//Space: o(n)
func merge(intervals [][]int) [][]int {
	res := [][]int{}
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})
	cur := intervals[0]

	for i := 0; i < len(intervals); i++ {
		if intervals[i][0] <= cur[1] {
			cur[1] = max(intervals[i][1], cur[1])
		} else {
			res = append(res, cur)
			cur = intervals[i]
		}
	}
	res = append(res, cur)
	return res

}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
