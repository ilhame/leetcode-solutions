package largest_number

import "testing"

func TestLargestNumber(t *testing.T) {
	tests := []struct {
		nums     []int
		expected string
	}{
		{[]int{10, 2}, "210"},
		{[]int{3, 30, 34, 5, 9}, "9534330"},
		{[]int{1}, "1"},
		{[]int{0, 0}, "0"},
		{[]int{9, 99, 998, 9997}, "9999997998"},
		{[]int{45, 42, 4, 40, 49}, "494544240"},
	}

	for _, test := range tests {
		result := largestNumber(test.nums)
		if result != test.expected {
			t.Errorf("For input %v, expected %v, but got %v", test.nums, test.expected, result)
		}
	}
}

// 49454240
// 49454240
