package largest_number

import (
	"sort"
	"strconv"
	"strings"
)

// Time: n*log(n)
// Space: o(n)
func largestNumber(nums []int) string {

	strNums := make([]string, len(nums))

	for i := 0; i < len(nums); i++ {
		strNums[i] = strconv.Itoa(nums[i])
	}

	sort.Slice(strNums, func(i, j int) bool {
		return strNums[i]+strNums[j] > strNums[j]+strNums[i]
	})

	if strNums[0] == "0" {
		return "0"
	}

	return strings.Join(strNums, "")

}
