package max_len_of_pair_chain

import (
	"sort"
)

func findLongestChain(pairs [][]int) int {
	sort.Slice(pairs, func(i, j int) bool {
		return pairs[i][1] < pairs[j][1]
	})
	cur := pairs[0]
	count := 1
	for i := 1; i < len(pairs); i++ {
		if pairs[i][0] > cur[1] {
			count++
			cur = pairs[i]
		}
	}
	return count
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
