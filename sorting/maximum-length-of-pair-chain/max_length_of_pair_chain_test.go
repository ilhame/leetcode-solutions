package max_len_of_pair_chain

import "testing"

func TestFindLongestChain(t *testing.T) {
	tests := []struct {
		pairs    [][]int
		expected int
	}{
		{
			[][]int{{1, 2}, {2, 3}, {3, 4}},
			2, // The longest chain is [1, 2] -> [3, 4]
		},
		{
			[][]int{{1, 2}, {2, 3}, {3, 4}, {5, 6}},
			3, // The longest chain is [1, 2] -> [3, 4] -> [5,6]
		},
		{
			[][]int{{1, 2}, {2, 3}, {3, 4}, {4, 5}},
			2, // The longest chain is [1, 2] -> [3, 4]
		},
		{
			[][]int{{-10, 8}, {2, 6}, {1, 3}, {4, 5}},
			2, // The longest chain is [1 3] -> [4 5]
		},
		{
			[][]int{{1, 2}},
			1, // Only one pair, so the longest chain is itself
		},
	}

	for _, test := range tests {
		result := findLongestChain(test.pairs)
		if result != test.expected {
			t.Errorf("For input %v, expected %v, but got %v", test.pairs, test.expected, result)
		}
	}
}
