package rottingoranges

func orangesRotting(grid [][]int) int {
	fresh := 0
	minutes := 0
	m, n := len(grid), len(grid[0])
	rotten := [][2]int{}
	directions := [4][2]int{{1, 0}, {-1, 0}, {0, 1}, {0, -1}}
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if grid[i][j] == 1 {
				fresh++
			} else if grid[i][j] == 2 {
				rotten = append(rotten, [2]int{i, j})
			}
		}
	}
	if fresh == 0 {
		return 0
	}
	for len(rotten) > 0 {
		for _, r := range rotten {
			for _, dir := range directions {
				x, y := r[0]+dir[0], r[1]+dir[1]
				if x >= 0 && x < m && y >= 0 && y < n && grid[x][y] == 1 {
					grid[x][y] = 2
					rotten = append(rotten, [2]int{x, y})
					fresh--
				}
			}
			rotten = rotten[1:]
		}
		minutes++
		if fresh == 0 {
			return minutes
		}

	}
	return -1

}
