package course_schedule_3

import (
	"container/heap"
	"sort"
)

type MaxHeap []int

func (mh MaxHeap) Len() int {
	return len(mh)
}
func (mh MaxHeap) Less(i, j int) bool {
	return mh[i] > mh[j]
}
func (mh MaxHeap) Swap(i, j int) {
	mh[i], mh[j] = mh[j], mh[i]
}
func (mh *MaxHeap) Pop() any {
	old := *mh
	n := len(old)
	x := old[n-1]
	*mh = old[0 : n-1]
	return x
}
func (mh MaxHeap) Peek() any {
	last := mh[0]
	return last
}
func (mh *MaxHeap) Push(val any) {
	*mh = append(*mh, val.(int))
}

// [[100,200],[200,1300],[1000,1250],[2000,3200]]
func scheduleCourse(courses [][]int) int {
	sort.Slice(courses, func(i, j int) bool {
		return courses[i][1] < courses[j][1]
	})
	maxHeap := &MaxHeap{}
	heap.Init(maxHeap)
	time := 0
	for _, course := range courses {
		if course[0] <= course[1] {
			if course[0]+time <= course[1] {
				time += course[0]
				heap.Push(maxHeap, course[0])
			} else {
				if maxHeap.Peek().(int) > course[0] {
					t := heap.Pop(maxHeap).(int)
					time -= t
					time += course[0]
					heap.Push(maxHeap, course[0])
				}
			}
		}

	}
	return maxHeap.Len()
}
