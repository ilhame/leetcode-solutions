package minimum_height_trees

func findMinHeightTrees(n int, edges [][]int) []int {
	if n == 1 {
		return []int{0}
	}
	list := map[int][]int{}
	for _, edge := range edges {
		u := edge[0]
		v := edge[1]
		list[u] = append(list[u], v)
		list[v] = append(list[v], u)
	}

	queue := []int{}

	for node, neighboor := range list {
		if len(neighboor) == 1 {
			queue = append(queue, node)
		}
	}

	for n > 2 {
		size := len(queue)

		for i := 0; i < size; i++ {
			node := queue[0]
			queue = queue[1:]

			neighboors := list[node]

			for i := 0; i < len(neighboors); i++ {
				list[neighboors[i]] = remove(list[neighboors[i]], node)

				if len(list[neighboors[i]]) == 1 {
					queue = append(queue, neighboors[i])
				}
			}

		}
		n -= size
	}

	return queue
}

func remove(nums []int, num int) []int {
	for i := 0; i < len(nums); i++ {
		if nums[i] == num {
			return append(nums[:i], nums[i+1:]...)
		}
	}
	return nums
}
