package minimum_height_trees

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestFindMinHeightTrees(t *testing.T) {
	testCases := []struct {
		n     int
		edges [][]int
		res   []int
	}{
		{
			4, [][]int{{1, 0}, {1, 2}, {1, 3}}, []int{1},
		},
		{
			6, [][]int{{3, 0}, {3, 1}, {3, 2}, {3, 4}, {5, 4}}, []int{3, 4},
		},
		{
			1, [][]int{}, []int{0},
		},
	}

	for _, testCase := range testCases {
		actual := findMinHeightTrees(testCase.n, testCase.edges)
		if !cmp.Equal(actual, testCase.res, cmpopts.SortSlices(func(x, y int) bool {
			return x < y
		})) {
			t.Errorf("for input %d and %v expected result vas %v but got %v", testCase.n, testCase.edges, testCase.res, actual)
		}

	}

}

type test struct {
	A int
	B string
}
