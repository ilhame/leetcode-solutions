package matrix

func updateMatrix(mat [][]int) [][]int {

	res := make([][]int, len(mat))
	for i := 0; i < len(mat); i++ {
		res[i] = make([]int, len(mat[i]))
	}
	queue := [][2]int{}

	for i := 0; i < len(res); i++ {
		for j := 0; j < len(res[i]); j++ {
			if mat[i][j] == 0 {
				queue = append(queue, [2]int{i, j})
			} else {
				res[i][j] = -1
			}

		}
	}

	m := len(mat)
	n := len(mat[0])
	for len(queue) > 0 {
		first := queue[0]
		i := first[0]
		j := first[1]
		if checkvalidity(i+1, j, m, n) && res[i+1][j] == -1 {
			queue = append(queue, [2]int{i + 1, j})
			res[i+1][j] = res[i][j] + 1
		}
		if checkvalidity(i-1, j, m, n) && res[i-1][j] == -1 {
			queue = append(queue, [2]int{i - 1, j})
			res[i-1][j] = res[i][j] + 1
		}
		if checkvalidity(i, j+1, m, n) && res[i][j+1] == -1 {
			queue = append(queue, [2]int{i, j + 1})
			res[i][j+1] = res[i][j] + 1
		}
		if checkvalidity(i, j-1, m, n) && res[i][j-1] == -1 {
			queue = append(queue, [2]int{i, j - 1})
			res[i][j-1] = res[i][j] + 1
		}
		queue = queue[1:]
	}
	return res
}

func checkvalidity(i, j, m, n int) bool {
	if i < 0 || j < 0 || i >= m || j >= n {
		return false
	}
	return true
}
