package course_schedule

func canFinish(numCourses int, prerequisites [][]int) bool {

	adj := map[int][]int{}
	indegrees := make([]int, numCourses)
	count := 0
	visited := make([]bool, numCourses)
	for _, prereq := range prerequisites {
		adj[prereq[1]] = append(adj[prereq[1]], prereq[0])
		indegrees[prereq[0]]++
	}
	queue := []int{}
	for i := 0; i < len(indegrees); i++ {
		if indegrees[i] == 0 {
			queue = append(queue, i)
			count++
			visited[i] = true
		}
	}

	for len(queue) > 0 {
		node := queue[0]

		n := adj[node]
		for i := 0; i < len(n); i++ {
			if !visited[n[i]] {
				indegrees[n[i]]--
				if indegrees[n[i]] == 0 {
					queue = append(queue, n[i])
					count++
					visited[n[i]] = true
				}
			}

		}

		queue = queue[1:]
	}
	return count == numCourses
}
