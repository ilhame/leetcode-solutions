package course_schedule_2

func findOrder(numCourses int, prerequisites [][]int) []int {
	adjList := map[int][]int{}
	indegree := make([]int, numCourses)
	for _, req := range prerequisites {
		adjList[req[1]] = append(adjList[req[1]], req[0])
		indegree[req[0]]++
	}
	queue := []int{}
	order := []int{}
	for i := 0; i < len(indegree); i++ {
		if indegree[i] == 0 {
			queue = append(queue, i)
			order = append(order, i)
		}
	}
	visited := make([]bool, numCourses)
	for len(queue) > 0 {
		size := len(queue)
		for i := 0; i < size; i++ {
			c := queue[i]
			visited[c] = true
			n := adjList[c]
			for i := 0; i < len(n); i++ {
				if !visited[n[i]] {
					indegree[n[i]]--
					if indegree[n[i]] == 0 {
						queue = append(queue, n[i])
						order = append(order, n[i])
					}
				}
			}
		}
		queue = queue[size:]
	}

	for _, v := range visited {
		if v == false {
			return []int{}
		}
	}

	return order
}
