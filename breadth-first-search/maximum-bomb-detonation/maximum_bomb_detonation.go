package maximumbombdetation

func maximumDetonation(bombs [][]int) int {
	adjList := map[int][]int{}
	for i := 0; i < len(bombs); i++ {
		for j := 0; j < len(bombs); j++ {
			if i != j {
				x1 := bombs[i][0]
				y1 := bombs[i][1]
				r1 := bombs[i][2]

				x2 := bombs[j][0]
				y2 := bombs[j][1]

				if (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) <= r1*r1 {
					adjList[i] = append(adjList[i], j)
				}
			}
		}
	}

	max := 0
	for i := 0; i < len(bombs); i++ {
		seen := map[int]bool{}
		q := []int{i}
		seen[i] = true
		for len(q) > 0 {
			first := q[0]
			q = q[1:]
			if val, ok := adjList[first]; ok {
				for _, neig := range val {
					if !seen[neig] {
						seen[neig] = true
						q = append(q, neig)
					}
				}
			}
		}
		if len(seen) == len(bombs) {
			return len(seen)
		}
		l := len(seen)
		if l > max {
			max = l
		}
	}

	return max

}
