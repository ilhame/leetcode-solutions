package climbing_stairs

import (
	"fmt"
	"testing"
)

func TestClimbStairs(t *testing.T) {
	testCases := []struct {
		n        int
		expected int
	}{
		{
			1, 1,
		},
		{
			2, 2,
		},
		{
			3, 3,
		},
		{
			4, 5,
		},
		{
			5, 8,
		},
		{
			10, 89,
		},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("n=%d", tc.n), func(t *testing.T) {
			actual := climbStairs(tc.n)
			if actual != tc.expected {
				t.Errorf("Expected %d, but got %d", tc.expected, actual)
			}
		})
	}
}
