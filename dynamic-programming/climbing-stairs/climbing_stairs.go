package climbing_stairs

// Time complexity O(n)
// Space complexity O(n)
func climbStairs(n int) int {
	dp := make([]int, n+2)
	dp[0] = 0
	dp[1] = 1
	dp[2] = 2
	if n <= 2 {
		return dp[n]
	}
	for i := 3; i <= n; i++ {
		dp[i] = dp[i-1] + dp[i-2]
	}
	return dp[n]
}

// Time complexity O(n)
// Space complexity O(1)
func climbStairsAlt(n int) int {
	a, b := 1, 1
	for n > 0 {
		a, b = b, a+b
		n--
	}
	return a
}

// This recursive approach is highly inefficient for large values of n and should be avoided in practice
// Space complexity O(n)
// The time complexity of this code is exponential, specifically O(2^n), as the number of function calls and computations doubles with each increment of n.
func climbStairsAlt2(n int) int {
	if n <= 3 {
		return n
	}
	return climbStairsAlt2(n-1) + climbStairsAlt2(n-2)
}
