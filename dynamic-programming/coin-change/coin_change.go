package coinchange

func coinChange(coins []int, amount int) int {
	dp := make([][]int, len(coins)+1)
	m, n := len(coins)+1, amount+1
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, amount+1)
	}
	dp[0][0] = 1
	for i := 0; i < n; i++ {
		dp[0][i] = amount + 1
	}

	for i := 1; i < m; i++ {
		for j := 1; j < n; j++ {
			if j < coins[i-1] {
				dp[i][j] = dp[i-1][j]
			} else {
				dp[i][j] = min(dp[i][j-coins[i-1]]+1, dp[i-1][j])
			}
		}
	}
	if dp[m-1][n-1] == amount+1 {
		return -1
	}
	return dp[m-1][n-1]
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
