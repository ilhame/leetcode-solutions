package meetingrooms

type Interval struct {
	Start, End int
}

// [(0,30),(5,10),(15,20)]
func CanAttendMeetings(intervals []*Interval) bool {
	// Write your code here
	for i := 1; i < len(intervals); i++ {
		if intervals[i].Start < intervals[i-1].End {
			return false
		}
	}
	return true
}
