package meetingrooms2

import (
	"container/heap"
	"sort"
)

type MinHeap []int

func (h *MinHeap) Len() int {
	return len(*h)
}

func (h *MinHeap) Less(i, j int) bool {
	return (*h)[i] < (*h)[j]
}

func (h *MinHeap) Pop() any {
	first := (*h)[0]
	*h = (*h)[:len(*h)-1]
	return first
}

func (h *MinHeap) Swap(i, j int) {
	(*h)[i], (*h)[j] = (*h)[j], (*h)[i]
}

func (h *MinHeap) Top() any {
	return (*h)[0]
}

func (h *MinHeap) Push(val any) {
	*h = append(*h, val.(int))
}

func solve(A [][]int) int {
	sort.Slice(A, func(i, j int) bool {
		return A[i][0] < A[j][0]
	})
	minHeap := &MinHeap{}
	heap.Init(minHeap)

	for i := 0; i < len(A); i++ {
		if minHeap.Len() < 1 {
			heap.Push(minHeap, A[i][1])
		} else {
			if minHeap.Top().(int) <= A[i][0] {
				heap.Pop(minHeap)
				heap.Push(minHeap, A[i][1])
			} else {
				heap.Push(minHeap, A[i][1])
			}
		}
	}
	return minHeap.Len()
}
