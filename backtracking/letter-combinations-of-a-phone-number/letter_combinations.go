package letter_combinations

func letterCombinations(digits string) []string {
	if digits == "" {
		return []string{}
	}
	res := []string{}
	phoneMap := []string{"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"}

	var backtrack func(combination, nextDigits string)
	backtrack = func(combination, nextDigits string) {
		if nextDigits == "" {
			res = append(res, combination)
		} else {
			letters := phoneMap[nextDigits[0]-'2']
			for _, letter := range letters {
				backtrack(combination+string(letter), nextDigits[1:])
			}
		}

	}

	backtrack("", digits)
	return res

}
