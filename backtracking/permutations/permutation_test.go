package permutations

import (
	"reflect"
	"testing"
)

func TestPermutate(t *testing.T) {
	testCases := []struct {
		input    []int
		expected [][]int
	}{
		{
			[]int{1, 2, 3},
			[][]int{{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1}},
		},
		{
			[]int{1},
			[][]int{{1}},
		},
	}

	for _, tc := range testCases {
		actual := permute(tc.input)
		if !reflect.DeepEqual(actual, tc.expected) {
			t.Errorf("For input %v expected %v but got %v", tc.input, tc.expected, actual)
		}
	}
}
