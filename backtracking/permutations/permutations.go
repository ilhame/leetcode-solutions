package permutations

func permute(nums []int) [][]int {
	res := [][]int{}
	temp := []int{}
	used := map[int]bool{}
	permutateHelper(&res, nums, temp, used)
	return res
}

func permutateHelper(res *[][]int, nums []int, permutation []int, used map[int]bool) {

	if len(permutation) == len(nums) {
		temp := make([]int, len(permutation))
		copy(temp, permutation)
		*res = append(*res, temp)
		return
	}

	for i := 0; i < len(nums); i++ {
		if !used[nums[i]] {
			used[nums[i]] = true
			permutation = append(permutation, nums[i])
			permutateHelper(res, nums, permutation, used)
			used[nums[i]] = false
			permutation = (permutation)[:len(permutation)-1]
		}

	}

}
