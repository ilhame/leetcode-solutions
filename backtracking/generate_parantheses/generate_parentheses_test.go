package generate_parentheses

import (
	"fmt"
	"reflect"
	"testing"
)

func TestGenerateParenthesis(t *testing.T) {
	testCases := []struct {
		n        int
		expected []string
	}{
		{1, []string{"()"}},
		{2, []string{"(())", "()()"}},
		{3, []string{"((()))", "(()())", "(())()", "()(())", "()()()"}},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("n=%d", tc.n), func(t *testing.T) {
			result := generateParenthesis(tc.n)
			if !reflect.DeepEqual(result, tc.expected) {
				t.Errorf("For n=%d, expected %v, but got %v", tc.n, tc.expected, result)
			}
		})
	}
}
