package generate_parentheses

func generateParenthesis(n int) []string {
	res := []string{}
	bytes := make([]byte, 2*n)
	generateParenthesisHelper(n, 0, 0, bytes, &res)
	return res
}

func generateParenthesisHelper(n, left, right int, bytes []byte, res *[]string) {
	if left == n && right == n {
		*res = append(*res, string(bytes))
		return
	}
	if left < n {
		bytes[left+right] = '('
		generateParenthesisHelper(n, left+1, right, bytes, res)
	}
	if right < left {
		bytes[left+right] = ')'
		generateParenthesisHelper(n, left, right+1, bytes, res)
	}
}
