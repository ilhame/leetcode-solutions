package kth_smallest_element_in_a_binary_tree

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func kthSmallest(root *TreeNode, k int) int {
	ch := make(chan int)
	go traverse(root, ch)

	for i := 0; i < k-1; i++ {
		<-ch
	}
	return <-ch
}

func traverse(root *TreeNode, ch chan int) {
	traverse(root.Left, ch)
	ch <- root.Val
	traverse(root.Right, ch)
}

func kthSmallestAlt(root *TreeNode, k int) int {
	res := []int{}
	inorderTraverse(root, &res)
	return 0
}

func inorderTraverse(root *TreeNode, res *[]int) {
	if root == nil {
		return
	}
	inorderTraverse(root.Left, res)
	*res = append(*res, root.Val)
	inorderTraverse(root.Right, res)
}
