package validate_binary_search_tree

import "math"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isValidBST(root *TreeNode) bool {
	if root == nil {
		return true
	}
	return isValidBSTUtil(root, math.MinInt, math.MaxInt)
}

func isValidBSTUtil(root *TreeNode, min, max int) bool {
	if root == nil {
		return true
	}
	if min <= root.Val && root.Val < max {
		return isValidBSTUtil(root.Left, min, root.Val) && isValidBSTUtil(root.Right, root.Val, max)
	} else {
		return false
	}
}
