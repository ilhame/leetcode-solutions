package invert_binary_tree

import "testing"

func TestInvertTree(t *testing.T) {
	testCases := []struct {
		input    *TreeNode
		expected *TreeNode
	}{
		{
			input: &TreeNode{
				Val: 4,
				Left: &TreeNode{
					Val:   2,
					Left:  &TreeNode{Val: 1},
					Right: &TreeNode{Val: 3},
				},
				Right: &TreeNode{
					Val:   7,
					Left:  &TreeNode{Val: 6},
					Right: &TreeNode{Val: 9},
				},
			},
			expected: &TreeNode{
				Val: 4,
				Left: &TreeNode{
					Val:   7,
					Left:  &TreeNode{Val: 9},
					Right: &TreeNode{Val: 6},
				},
				Right: &TreeNode{
					Val:   2,
					Left:  &TreeNode{Val: 3},
					Right: &TreeNode{Val: 1},
				},
			},
		},
	}

	for _, tc := range testCases {
		actualTree := invertTree(tc.input)
		if !IsTreeEqual(actualTree, tc.expected) {
			t.Error("expected inverted tree doesn't match the actual inverted tree")
		}
	}

}

func IsTreeEqual(tree1 *TreeNode, tree2 *TreeNode) bool {
	if tree1 == nil && tree2 == nil {
		return true
	}
	if tree1 == nil || tree2 == nil {
		return false
	}

	return tree1.Val == tree2.Val && IsTreeEqual(tree1.Left, tree2.Left) && IsTreeEqual(tree1.Right, tree2.Right)
}
