package symmetric_tree

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isSymmetric(root *TreeNode) bool {

	if root == nil {
		return true
	}
	return check(root.Left, root.Right)

}

func check(left *TreeNode, right *TreeNode) bool {
	if left == nil && right == nil {
		return true
	} else if left != nil && right != nil {
		return left.Val == right.Val && check(left.Left, right.Right) && check(left.Right, right.Left)
	} else {
		return false
	}
}
