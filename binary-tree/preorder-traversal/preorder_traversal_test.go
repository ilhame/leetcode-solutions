package preorder_traversal

import (
	"reflect"
	"testing"
)

func TestPreorderTraversal(t *testing.T) {
	testCases := []struct {
		name     string
		input    *TreeNode
		expected []int
	}{
		{
			name: "example case",
			input: &TreeNode{
				Val: 1,
				Right: &TreeNode{
					Val:  2,
					Left: &TreeNode{Val: 3},
				},
			},
			expected: []int{1, 2, 3},
		},
		{
			name:     "Empty Tree",
			input:    nil,
			expected: []int{},
		},
		{
			name:     "Single Node",
			input:    &TreeNode{Val: 5},
			expected: []int{5},
		},
		{
			name: "Complex Tree",
			input: &TreeNode{
				Val: 1,
				Left: &TreeNode{
					Val:   2,
					Left:  &TreeNode{Val: 4},
					Right: &TreeNode{Val: 5},
				},
				Right: &TreeNode{
					Val:   3,
					Left:  &TreeNode{Val: 6},
					Right: &TreeNode{Val: 7},
				},
			},
			expected: []int{1, 2, 4, 5, 3, 6, 7},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			actual := preorderTraversal(tc.input)
			if !reflect.DeepEqual(actual, tc.expected) {
				t.Errorf("For test case %s, expected %v but got %v", tc.name, tc.expected, actual)

			}
		})
	}
}
