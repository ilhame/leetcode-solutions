package diameter_of_binary_tree

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func diameterOfBinaryTree(root *TreeNode) int {
	if root == nil {
		return 0
	}
	max := 0
	diameterOfBinaryTreeUtil(root, &max)
	return max
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func diameterOfBinaryTreeUtil(root *TreeNode, max *int) int {
	if root == nil {
		return 0
	}
	left := diameterOfBinaryTreeUtil(root.Left, max)
	right := diameterOfBinaryTreeUtil(root.Right, max)

	height := max(left, right) + 1
	if left+right > *max {
		*max = left + right
	}
	return height
}
