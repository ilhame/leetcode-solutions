package convertsortedlisttobinarysearchtree

type ListNode struct {
	Val  int
	Next *ListNode
}

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func sortedListToBST(head *ListNode) *TreeNode {
	cur := head
	nums := []int{}
	for cur != nil {
		nums = append(nums, cur.Val)
		cur = cur.Next
	}

	return fnc(nums, 0, len(nums)-1)

}

func fnc(nums []int, l, r int) *TreeNode {
	if l > r {
		return nil
	}
	mid := l + (r-l)/2
	node := &TreeNode{Val: nums[mid]}
	node.Left = fnc(nums, l, mid-1)
	node.Right = fnc(nums, mid+1, r)
	return node
}
