package binary_tree_level_order_traversal

type TreeNode struct {
	Val         int
	Left, Right *TreeNode
}

func levelOrder(root *TreeNode) [][]int {
	return bfs(root)
}

func bfs(root *TreeNode) [][]int {
	if root == nil {
		return [][]int{}
	}

	var result [][]int
	queue := []*TreeNode{root, nil}
	temp := []int{}

	for len(queue) > 0 {
		currentNode := queue[0]
		queue = queue[1:]

		if currentNode != nil {
			temp = append(temp, currentNode.Val)

			if currentNode.Left != nil {
				queue = append(queue, currentNode.Left)
			}
			if currentNode.Right != nil {
				queue = append(queue, currentNode.Right)
			}
		} else {
			result = append(result, temp)
			temp = []int{}

			if len(queue) > 0 {
				queue = append(queue, nil)
			}
		}
	}

	return result
}
