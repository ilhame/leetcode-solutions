package maximum_depth_of_binary_tree

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func MaxDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}

	leftDepth := MaxDepth(root.Left)
	rightDepth := MaxDepth(root.Right)

	// The maximum depth of the tree is the maximum of left and right subtree depths,
	// plus 1 for the current node.
	if leftDepth > rightDepth {
		return leftDepth + 1
	}
	return rightDepth + 1
}

func MaxmaxDepthAlt(root *TreeNode) int {
	if root == nil {
		return 0
	}
	queue := []*TreeNode{}
	queue = append(queue, root)
	level := 0
	for len(queue) > 0 {
		size := len(queue)
		level++
		for i := 0; i < size; i++ {
			node := queue[0]

			if node.Left != nil {
				queue = append(queue, node.Left)
			}
			if node.Right != nil {
				queue = append(queue, node.Right)
			}
			queue = queue[1:]
		}

	}
	return level
}
