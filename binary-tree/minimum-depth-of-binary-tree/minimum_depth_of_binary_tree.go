package minimumdepthofbinarytree

import "math"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

// minDepth implemented with DFS
func minDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}
	depth := math.MaxInt
	if root.Left != nil {
		depth = min(depth, minDepth(root.Left))
	}
	if root.Right != nil {
		depth = min(depth, minDepth(root.Left))
	}
	return 1 + depth
}

// minDepth implemented with BFS
func minDepthAlt(root *TreeNode) int {
	if root == nil {
		return 0
	}
	depth := 0
	queue := []*TreeNode{root}
	for len(queue) > 0 {
		size := len(queue)
		depth++
		for i := 0; i < size; i++ {
			node := queue[i]
			if node.Left == nil && node.Right == nil {
				return depth
			}
			if node.Left != nil {
				queue = append(queue, node.Left)
			}
			if node.Right != nil {
				queue = append(queue, node.Right)
			}
		}
		queue = queue[size:]
	}
	return depth
}

func minDepthAlt2(root *TreeNode) int {
	if root == nil {
		return 0
	}
	left := minDepth(root.Left)
	right := minDepth(root.Right)

	if left == 0 || right == 0 {
		return left + right + 1
	}

	return 1 + min(left, right)
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
