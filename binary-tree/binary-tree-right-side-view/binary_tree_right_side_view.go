package binary_tree_right_side_view

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func rightSideView(root *TreeNode) []int {
	if root == nil {
		return nil
	}
	queue := []*TreeNode{}
	queue = append(queue, root)
	queue = append(queue, nil)
	temp := []int{}
	res := []int{}
	for len(queue) > 0 {

		node := queue[0]

		if node != nil {
			queue = queue[1:]
			if node.Left != nil {
				queue = append(queue, node.Left)
			}
			if node.Right != nil {
				queue = append(queue, node.Right)
			}

			temp = append(temp, node.Val)
		} else {
			queue = queue[1:]

			res = append(res, temp[len(temp)-1])

			temp = []int{}
			if len(queue) > 0 {
				queue = append(queue, nil)
			}

		}

	}
	return res
}
