package binary_tree_tilt

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func FindTilt(root *TreeNode) int {
	var res int
	if root == nil {
		return 0
	} else {
		sum(root, &res)
	}

	return res
}

func sum(root *TreeNode, res *int) int {
	if root == nil {
		return 0
	}
	left, right := 0, 0
	if root.Left != nil {
		left = sum(root.Left, res)
	}
	if root.Right != nil {
		right = sum(root.Right, res)
	}
	*res += dif(left, right)
	return root.Val + left + right
}

func dif(a, b int) int {
	if a > b {
		return a - b
	} else {
		return b - a
	}
}
