package lowest_common_ancestor

import "testing"

func TestLowestCommonAncestor(t *testing.T) {

	testCases := []struct {
		name       string
		root, p, q *TreeNode
		expected   *TreeNode
	}{
		{
			root: &TreeNode{Val: 6, Left: &TreeNode{Val: 2, Left: &TreeNode{Val: 0}, Right: &TreeNode{Val: 4, Left: &TreeNode{Val: 3}, Right: &TreeNode{Val: 5}}},
				Right: &TreeNode{Val: 8, Left: &TreeNode{Val: 7}, Right: &TreeNode{Val: 9}},
			},
			p:        &TreeNode{Val: 2},
			q:        &TreeNode{Val: 8},
			expected: &TreeNode{Val: 6},
		},
		{
			root: &TreeNode{Val: 3, Left: &TreeNode{Val: 6, Left: &TreeNode{Val: 2}, Right: &TreeNode{Val: 11, Left: &TreeNode{Val: 9}, Right: &TreeNode{Val: 5}}},
				Right: &TreeNode{Val: 8, Right: &TreeNode{Val: 13, Left: &TreeNode{Val: 7}}}},
			p:        &TreeNode{Val: 2},
			q:        &TreeNode{Val: 5},
			expected: &TreeNode{Val: 6},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			result := lowestCommonAncestor(testCase.root, testCase.p, testCase.q)
			if result != nil && result.Val != testCase.expected.Val {
				t.Errorf("For root=%v, p=%v, q=%v, expected=%v, but got=%v", testCase.root, testCase.p, testCase.q, testCase.expected, result)
			}
		})
	}

}
