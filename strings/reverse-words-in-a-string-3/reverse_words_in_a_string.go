package reverse_words_in_a_string_3

func reverseWords(s string) string {
	runes := []rune(s)
	lastSpaceIdx := -1
	for i := 0; i <= len(runes); i++ {
		if i == len(runes) || s[i] == ' ' {
			l := lastSpaceIdx + 1
			r := i - 1
			for l < r {
				runes[l], runes[r] = runes[r], runes[l]
				l++
				r--
			}
			lastSpaceIdx = i
		}
	}
	return string(runes)
}
