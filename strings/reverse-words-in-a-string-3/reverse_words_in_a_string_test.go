package reverse_words_in_a_string_3

import "testing"

func TestReverseWords(t *testing.T) {
	testcases := []struct {
		input    string
		expected string
	}{
		{
			"Let's take LeetCode contest",
			"s'teL ekat edoCteeL tsetnoc",
		},
		{
			"God Ding",
			"doG gniD",
		},
	}

	for _, tc := range testcases {
		actual := reverseWords(tc.input)
		if tc.expected != actual {
			t.Errorf("for input %s expected %s but got %s", tc.input, tc.expected, actual)
		}
	}
}
