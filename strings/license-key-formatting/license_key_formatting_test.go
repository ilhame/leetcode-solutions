package licence_key_formatting

import "testing"

func TestLicenseKeyFormatting(t *testing.T) {
	testcases := []struct {
		s        string
		k        int
		expected string
	}{
		{
			"5F3Z-2e-9-w",
			4,
			"5F3Z-2E9W",
		},
		{
			"2-5g-3-J",
			2,
			"2-5G-3J",
		},
		{
			"a-a-a-a-",
			1,
			"A-A-A-A",
		},
	}

	for _, tc := range testcases {
		res := licenseKeyFormatting(tc.s, tc.k)
		if res != tc.expected {
			t.Errorf("For input  %s %d  expected  %s  but got %s ", tc.s, tc.k, tc.expected, res)
		}
	}
}
