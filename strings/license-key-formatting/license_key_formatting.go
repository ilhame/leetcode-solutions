package licence_key_formatting

import "strings"

func licenseKeyFormatting(s string, k int) string {
	s = strings.ToUpper(s)
	s = strings.Replace(s, "-", "", -1)
	var sb strings.Builder
	count := 0
	for i := len(s) - 1; i >= 0; i-- {
		if count == k {
			sb.WriteByte('-')
			count = 0
		}
		sb.WriteByte(s[i])
		count++

	}
	return reverse(sb.String())
}

func reverse(s string) string {
	runes := []rune(s)
	l, r := 0, len(runes)-1
	for l < r {
		runes[l], runes[r] = runes[r], runes[l]
		l++
		r--
	}
	return string(runes)
}
