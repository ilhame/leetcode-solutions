package add_binary

// Time complexity O(max(len(a), len(b))).
// Space complexity O(max(len(a), len(b)))
// The space complexity of the code is determined by the space used for variables and the result slice.
// The result slice is allocated with a maximum length of max(len(a), len(b)) + 1, which ensures that it can accommodate the result without reallocation.
// Other variables like lenA, lenB, carry, digitA, digitB, total, and the loop index are all integer variables and have constant space complexity, O(1).
// Therefore, the space complexity is O(max(len(a), len(b)) + 1), which simplifies to O(max(len(a), len(b))).
func addBinary(a string, b string) string {
	lenA, lenB := len(a), len(b)
	maxLen := max(lenA, lenB)
	carry := 0
	result := make([]byte, maxLen+1)

	for i := 0; i <= maxLen; i++ {
		digitA, digitB := 0, 0
		if i < lenA {
			digitA = int(a[lenA-1-i] - '0')
		}
		if i < lenB {
			digitB = int(b[lenB-1-i] - '0')
		}

		total := digitA + digitB + carry
		result[maxLen-i] = byte(total%2 + '0')
		carry = total / 2
	}

	if result[0] == '0' {
		result = result[1:]
	}

	return string(result)
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
