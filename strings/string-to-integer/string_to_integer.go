package string_to_integer

import "math"

func myAtoi(s string) int {
	num := 0
	i := 0
	for i < len(s) && (s[i] == ' ' || s[i] == '\t') {
		i++
	}
	if i >= len(s) {
		return 0
	}

	isNegative := 1
	if s[i] == '-' {
		isNegative = -1
		i++
	} else if s[i] == '+' {
		i++
	}
	//4193 with words
	for i < len(s) && (s[i] >= '0' && s[i] <= '9') {
		digit := int(s[i] - '0')
		if num*isNegative > math.MaxInt32/10 || num*isNegative == math.MaxInt32/10 && digit > 7 {
			return math.MaxInt32
		}

		if num*isNegative < math.MinInt32/10 || num*isNegative == math.MinInt32/10 && digit > 7 {
			return math.MinInt32
		}

		num = num*10 + digit
		i++
	}
	return isNegative * num

}
