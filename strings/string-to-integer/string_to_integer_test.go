package string_to_integer

import "testing"

func TestMyAtoi(t *testing.T) {
	testCases := []struct {
		input    string
		expected int
	}{
		{
			"42",
			42,
		},
		{
			"   -42",
			-42,
		},
		{
			"4193 with words",
			4193,
		},
		{
			"-91283472332",
			-2147483648,
		},
		{
			"",
			0,
		},
		{
			"    	",
			0,
		},
		{
			"     +004500",
			4500,
		},
		{
			"2147483648",
			2147483647,
		},
	}

	for _, tc := range testCases {
		res := myAtoi(tc.input)
		if res != tc.expected {
			t.Errorf("for input %s expected %d but got %d", tc.input, tc.expected, res)
		}
	}
}
