package encodeanddecodestrings

import "strconv"

func Encode(strs []string) string {
	res := ""
	for i := 0; i < len(strs); i++ {
		res += strconv.Itoa(len((strs[i]))) + "#" + strs[i]
	}
	return res
}

func Decode(str string) []string {
	i := 0
	res := []string{}
	for i < len(str) {
		j := i
		for j < len(str) {
			if str[i] != '#' {
				j++
			}
		}

		res = append(res, str[j+1:])

	}
	return res
}
