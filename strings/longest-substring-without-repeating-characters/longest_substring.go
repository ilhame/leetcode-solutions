package longest_substring

// time complexity of the code is O(n), and the space complexity is O(1).
func lengthOfLongestSubstring(s string) int {
	seen := map[byte]bool{}

	i := 0
	left := 0
	maxL := 0
	for i < len(s) {
		if !seen[s[i]] {
			seen[s[i]] = true
			i++
			maxL = max(maxL, len(seen))
		} else {
			delete(seen, s[left])
			left++
		}
	}
	return maxL

}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
