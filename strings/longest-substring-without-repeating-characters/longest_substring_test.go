package longest_substring

import "testing"

func TestLengthOfLongestSubstring(t *testing.T) {
	testCases := []struct {
		s        string
		expected int
	}{
		{
			"abcabcbb",
			3,
		},
		{
			"bbbb",
			1,
		},
		{
			"pwwkew",
			3,
		},
	}

	for _, tc := range testCases {
		actual := lengthOfLongestSubstring(tc.s)
		if actual != tc.expected {
			t.Errorf("expected %d but got %d", tc.expected, actual)
		}
	}
}
