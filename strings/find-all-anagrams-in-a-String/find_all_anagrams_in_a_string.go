package find_all_anagrams_in_a_string

func isAnagram(s string, t string) bool {

	alphabet := make([]int, 26)

	for _, v := range s {
		alphabet[v-'a']++
	}

	for _, v := range t {
		alphabet[v-'a']--
	}

	for _, v := range alphabet {
		if v != 0 {
			return false
		}
	}
	return true
}

func findAnagrams(s string, p string) []int {
	res := []int{}
	for i := 0; i < len(s); i++ {
		if i+len(p) <= len(s) {
			if isAnagram(s[i:i+len(p)], p) {
				res = append(res, i)
			}
		}
	}
	return res
}
