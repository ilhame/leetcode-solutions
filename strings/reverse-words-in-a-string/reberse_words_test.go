package reverse_words_in_a_string

import "testing"

// Input: s = "the sky is blue"
// Output: "blue is sky the"
func TestReverseWords(t *testing.T) {
	testcases := []struct {
		input    string
		expected string
	}{
		{
			"the sky is blue",
			"blue is sky the",
		},
		{
			"  hello world  ",
			"world hello",
		},
		{
			"a good   example",
			"example good a",
		},
	}

	for _, tc := range testcases {
		res := reverseWords(tc.input)
		if res != tc.expected {
			t.Errorf("for input %s expected %s but got %s", tc.input, tc.expected, res)
		}
	}
}
