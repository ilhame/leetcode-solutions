package reverse_words_in_a_string

import "strings"

func reverseWordsAlt(s string) string {
	words := strings.Fields(s)
	res := make([]string, len(words))
	for i := len(words) - 1; i >= 0; i-- {
		res[len(words)-1-i] = words[i]
	}
	return strings.Join(res, " ")
}

func reverseWords(s string) string {
	words := strings.Fields(s)
	reverse(words)
	return strings.Join(words, " ")
}

func reverse(arr []string) {
	l, r := 0, len(arr)-1
	for l < r {
		arr[l], arr[r] = arr[r], arr[l]
		l++
		r--
	}
}
