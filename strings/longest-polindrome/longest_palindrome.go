package longest_polindrome

// Time complexity O(n)
// Space complexity O(n)
// longestPalindrome the length of the longest palindrome that can be built with those letters.
func longestPalindrome(s string) int {
	count := map[byte]int{}
	for i := 0; i < len(s); i++ {
		count[s[i]]++
	}
	res := 0
	isOdd := false
	for _, v := range count {
		if v%2 == 0 {
			res += v
		} else {
			isOdd = true
			res += v - 1
		}
	}
	if isOdd {
		res++
	}
	return res
}
