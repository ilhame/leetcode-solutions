package largest_rectangle_in_histogram

type MyStack struct {
	arr []int
}

func (s *MyStack) Push(val int) {
	s.arr = append(s.arr, val)
}

func (s *MyStack) Empty() bool {
	return len(s.arr) == 0
}

func (s *MyStack) Pop() int {
	last := s.arr[len(s.arr)-1]
	s.arr = s.arr[:len(s.arr)-1]
	return last
}

func (s *MyStack) Top() int {
	return s.arr[len(s.arr)-1]
}

func largestRectangleArea(heights []int) int {
	size := len(heights)
	left := make([]int, size)
	right := make([]int, size)
	stack := &MyStack{}
	stack2 := &MyStack{}

	// Calculate 'left' array
	for i := 0; i < size; i++ {
		for !stack.Empty() && heights[i] <= heights[stack.Top()] {
			stack.Pop()
		}
		if stack.Empty() {
			left[i] = 0
		} else {
			left[i] = stack.Top() + 1
		}
		stack.Push(i)
	}

	// Calculate 'right' array
	for i := size - 1; i >= 0; i-- {
		for !stack2.Empty() && heights[i] <= heights[stack2.Top()] {
			stack2.Pop()
		}
		if stack2.Empty() {
			right[i] = size - 1
		} else {
			right[i] = stack2.Top() - 1
		}
		stack2.Push(i)
	}

	// Calculate max area
	maxArea := 0
	for i := 0; i < size; i++ {
		area := (right[i] - left[i] + 1) * heights[i]
		if area > maxArea {
			maxArea = area
		}
	}

	return maxArea
}
