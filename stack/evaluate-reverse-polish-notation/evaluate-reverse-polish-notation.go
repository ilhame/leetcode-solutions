package evaluate_reverse_polish_notation

import (
	"log"
	"strconv"
)

type Stack []int

func (s *Stack) Push(val int) {
	*s = append(*s, val)
}

func (s *Stack) Pop() int {
	lastIdx := len(*s) - 1
	val := (*s)[lastIdx]
	*s = (*s)[:lastIdx]
	return val
}

func evalRPN(tokens []string) int {
	var stack Stack
	for _, token := range tokens {

		switch token {
		case "+", "-", "*", "/":
			first := stack.Pop()
			second := stack.Pop()
			var res int

			if token == "+" {
				res = second + first
			} else if token == "-" {
				res = second - first
			} else if token == "*" {
				res = second * first
			} else {
				res = second / first
			}
			stack.Push(res)
		default:
			stack.Push(ToInt(token))
		}

	}
	return stack.Pop()
}

func ToInt(s string) int {
	res, err := strconv.Atoi(s)
	if err != nil {
		log.Println(err)
	}
	return res
}

func evalRPNAlt(tokens []string) int {
	operators := map[string]func(a, b int) int{
		"+": func(a, b int) int { return a + b },
		"-": func(a, b int) int { return a - b },
		"*": func(a, b int) int { return a * b },
		"/": func(a, b int) int { return a / b },
	}

	var stack Stack

	for _, token := range tokens {
		if calculate, exists := operators[token]; exists {
			a := stack.Pop()
			b := stack.Pop()
			stack.Push(calculate(b, a))
		} else {
			stack.Push(ToInt(token))
		}
	}
	return stack.Pop()
}
