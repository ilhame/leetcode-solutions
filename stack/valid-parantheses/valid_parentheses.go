package valid_parentheses

type Stack []rune

func (s *Stack) Push(val rune) {
	*s = append(*s, val)
}

func (s *Stack) Pop() rune {
	last := len(*s) - 1
	item := (*s)[last]
	*s = (*s)[:last]
	return item
}

// time complexity of the function is O(n)
// space complexity of the function is O(n)
func isValidAlt(s string) bool {
	var stack Stack

	for i, ch := range s {
		if s[i] == '(' || s[i] == '{' || s[i] == '[' {
			stack.Push(ch)
		} else if s[i] == ')' && (len(stack) == 0 || stack.Pop() != '(') {
			return false
		} else if s[i] == '}' && (len(stack) == 0 || stack.Pop() != '{') {
			return false
		} else if s[i] == ']' && (len(stack) == 0 || stack.Pop() != '[') {
			return false
		}

	}
	return len(stack) == 0
}

func isValid(s string) bool {
	var stack Stack

	for _, ch := range s {
		if ch == '(' {
			stack.Push(')')
		} else if ch == '[' {
			stack.Push(']')
		} else if ch == '{' {
			stack.Push('}')
		} else if len(stack) == 0 || stack.Pop() != ch {

			return false

		}

	}
	return len(stack) == 0
}

// The time complexity of the provided code is O(n), where n is the length of the input string s.
//This is because the code iterates through each character of the string once in a single pass.
//Within the loop, each operation such as appending to the stack, accessing elements, and slicing the stack has constant
// time complexity. Therefore, the overall time complexity is linear with respect to the length of the input string.

// The space complexity of the code is also O(n), where n is the length of the input string s.
// This is because the code uses a stack data structure to keep track of the opening brackets encountered so far.
// In the worst case, the stack may contain all opening brackets from the input string, resulting in space proportional
// to the length of the input string. Additionally, the map pairs occupies constant space as it contains a fixed number
// of key-value pairs regardless of the input size. Therefore, the overall space complexity is linear with respect to the length of the input string.
func isValidAlt2(s string) bool {
	stack := []rune{}
	pairs := map[rune]rune{
		'(': ')',
		'{': '}',
		'[': ']',
	}
	for _, r := range s {
		if _, ok := pairs[r]; ok {
			stack = append(stack, r)
		} else if len(stack) == 0 || pairs[stack[len(stack)-1]] != r {
			return false
		} else {
			stack = stack[:len(stack)-1]
		}
	}
	return len(stack) == 0
}
