package valid_parentheses

import "testing"

func TestIsValid(t *testing.T) {
	testCases := []struct {
		input    string
		expected bool
	}{
		{"()", true},
		{"()[]{}", true},
		{"(]", false},
		{"([)]", false},
		{"{[]}", true},
		{"[", false},
		{"]", false},
	}

	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			result := isValid(tc.input)
			if result != tc.expected {
				t.Errorf("For input: %s expected %v, but got %v", tc.input, tc.expected, result)
			}
		})
	}
}
