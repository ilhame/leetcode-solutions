package implement_queue_using_stack

type MyQueue struct {
	pushStack, popStack []int
}

func Constructor() MyQueue {
	return MyQueue{}
}

func (this *MyQueue) Push(x int) {
	this.pushStack = append(this.pushStack, x)
}

func (this *MyQueue) Pop() int {
	ans := this.Peek()
	this.popStack = this.popStack[:len(this.popStack)-1]
	return ans

}

func (this *MyQueue) Peek() int {
	if len(this.popStack) == 0 {
		last := this.pushStack[len(this.pushStack)-1]
		this.pushStack = this.pushStack[:len(this.pushStack)-1]
		this.popStack = append(this.popStack, last)
	}

	return this.popStack[len(this.popStack)-1]
}

func (this *MyQueue) Empty() bool {
	return len(this.pushStack) == 0 && len(this.popStack) == 0
}
