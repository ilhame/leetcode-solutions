package min_stack

import "testing"

func TestMinStack(t *testing.T) {
	testCases := []struct {
		operations []string
		values     []int
		expected   []int
	}{
		{
			operations: []string{"Push", "Push", "Push", "GetMin", "Pop", "Top", "GetMin"},
			values:     []int{-2, 0, -3, -3, 0, 0, -2},
			expected:   []int{0, 0, 0, -3, 0, 0, -2},
		},
	}

	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			stack := Constructor()

			for i, op := range tc.operations {
				val := tc.values[i]
				switch op {
				case "Push":
					stack.Push(val)
				case "Pop":
					stack.Pop()
				case "Top":
					result := stack.Top()
					if result != val {
						t.Errorf("Expected Top() %d, but got %d", val, result)
					}
				case "GetMin":
					result := stack.GetMin()
					if result != val {
						t.Errorf("Expected GetMin() %d, but got %d", val, result)
					}
				}
			}
		})
	}
}
