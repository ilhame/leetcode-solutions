package min_stack

type MinStack struct {
	arr []int
	min []int
}

func Constructor() MinStack {
	return MinStack{
		arr: make([]int, 0),
		min: make([]int, 0),
	}
}

func (this *MinStack) Push(val int) {
	this.arr = append(this.arr, val)
	if len(this.min) == 0 {
		this.min = append(this.min, val)
	} else {
		minVal := minimum(val, this.min[len(this.min)-1])
		this.min = append(this.min, minVal)
	}

}

func (this *MinStack) Pop() {
	this.arr = this.arr[:len(this.arr)-1]
	this.min = this.min[:len(this.min)-1]
}

func (this *MinStack) Top() int {
	lastIdx := len(this.arr) - 1
	return this.arr[lastIdx]
}

func (this *MinStack) GetMin() int {
	return this.min[len(this.min)-1]
}

func minimum(a, b int) int {
	if a < b {
		return a
	}
	return b
}
