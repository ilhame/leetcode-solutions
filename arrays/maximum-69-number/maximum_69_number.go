package maximum69number

func maximum69Number(num int) int {
	digits := []int{}
	for num != 0 {
		digits = append(digits, num%10)
		num = num / 10
	}
	used := false
	res := 0
	for i := len(digits) - 1; i >= 0; i-- {
		if digits[i] == 6 && !used {
			digits[i] = 9
			used = true
		}
		res = res*10 + digits[i]
	}
	return res
}
