package maximum_subarray

// an implementation of the Kadane's algorithm to find the maximum subarray sum
// Time Complexity: O(n) where 'n' is the length of the input array nums.
// Space Complexity: O(1), constant space complexity.
func maxSubArray(nums []int) int {
	sum, curSum := nums[0], 0
	for i := 0; i < len(nums); i++ {
		if curSum < 0 {
			curSum = 0
		}
		curSum += nums[i]
		if curSum > sum {
			sum = curSum
		}

	}
	return sum
}
