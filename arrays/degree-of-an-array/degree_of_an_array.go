package degreeofanarray

func findShortestSubArray(nums []int) int {
	seen := map[int]int{}
	firstSeen := map[int]int{}
	degree := 0
	minLen := 0
	for i := 0; i < len(nums); i++ {
		seen[nums[i]]++
		if _, ok := firstSeen[nums[i]]; !ok {
			firstSeen[nums[i]] = i
		}
		if seen[nums[i]] > degree {
			degree = seen[nums[i]]
			minLen = i - firstSeen[nums[i]] + 1
		} else if seen[nums[i]] == degree {
			degree = seen[nums[i]]
			minLen = min(minLen, i-firstSeen[nums[i]]+1)
		}
	}
	return minLen

}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
