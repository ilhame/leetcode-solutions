package majority_element

// Time complexity O(n)
// Space complexity O(1)
// The algorithm is based on the Boyer-Moore Majority Vote algorithm.
func majorityElement(nums []int) int {
	target := nums[0]
	count := 1

	for i := 1; i < len(nums); i++ {
		if nums[i] == target {
			count++
		} else {
			count--
			if count == 0 {
				target = nums[i]
				count = 1
			}

		}
	}
	return target
}
