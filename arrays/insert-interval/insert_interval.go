package insert_interval

// Time Complexity: O(n)
// Space Complexity: O(n)
func insert(intervals [][]int, newInterval []int) [][]int {
	res := make([][]int, 0)
	i := 0

	// Add all intervals that come before newInterval
	for i < len(intervals) && intervals[i][1] < newInterval[0] {
		res = append(res, intervals[i])
		i++
	}

	// Merge overlapping intervals
	for i < len(intervals) && intervals[i][0] <= newInterval[1] {
		newInterval[0] = min(newInterval[0], intervals[i][0])
		newInterval[1] = max(newInterval[1], intervals[i][1])
		i++
	}
	res = append(res, newInterval)

	// Add all remaining intervals after newInterval
	for i < len(intervals) {
		res = append(res, intervals[i])
		i++
	}

	return res
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
