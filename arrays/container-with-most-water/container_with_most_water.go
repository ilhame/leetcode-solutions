package main

import "fmt"

func main() {
	fmt.Println(maxArea([]int{1, 1}))
}

func maxArea(height []int) int {
	maxArea := 0
	l, r := 0, len(height)-1
	for l < r {
		if height[l] < height[r] {
			maxArea = max(maxArea, (r-l)*min(height[l], height[r]))
			l++
		} else {
			maxArea = max(maxArea, (r-l)*min(height[l], height[r]))
			r--
		}
	}
	return maxArea

}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
func maxArea2(height []int) int {
	maxArea := 0
	for i := 0; i < len(height); i++ {
		for j := i + 1; j < len(height); j++ {
			width := j - i
			area := width * min(height[i], height[j])
			maxArea = max(maxArea, area)
		}
	}
	return maxArea

}
