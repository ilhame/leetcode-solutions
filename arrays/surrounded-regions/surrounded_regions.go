package surroundedregions

func solve(board [][]byte) {
	if len(board) == 0 {
		return
	}
	m, n := len(board), len(board[0])
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if board[i][j] == 'O' && (i == 0 || i == m-1 || j == 0 || j == n-1) {
				capture(i, j, m, n, board)
			}
		}
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if board[i][j] == 'O' {
				board[i][j] = 'X'
			}
		}
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if board[i][j] == 'T' {
				board[i][j] = 'O'
			}
		}
	}
}

func capture(i, j, m, n int, board [][]byte) {
	if i < 0 || j < 0 || i == m || j == n || board[i][j] != 'O' {
		return
	}
	board[i][j] = 'T'
	capture(i+1, j, m, n, board)
	capture(i-1, j, m, n, board)
	capture(i, j+1, m, n, board)
	capture(i, j-1, m, n, board)

}
