package sort_colors

import (
	"reflect"
	"testing"
)

func TestSortColors(t *testing.T) {
	testCases := []struct {
		input    []int
		expected []int
	}{
		{
			[]int{2, 0, 2, 1, 1, 0},
			[]int{0, 0, 1, 1, 2, 2},
		},
		{
			[]int{2, 0, 1},
			[]int{0, 1, 2},
		},
	}

	for _, tc := range testCases {
		inputCopy := make([]int, len(tc.input))
		copy(inputCopy, tc.input)
		sortColors(inputCopy)
		if !reflect.DeepEqual(inputCopy, tc.expected) {
			t.Errorf("For input %v expected %v but got %d", tc.input, tc.expected, inputCopy)
		}
	}
}
