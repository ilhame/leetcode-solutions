package sort_colors

func sortColorsAlt(nums []int) {
	left := 0
	for i := range nums {
		if nums[i] == 0 {
			nums[left], nums[i] = nums[i], nums[left]
			left++
		}
	}
	right := len(nums) - 1
	for i := range nums {
		if nums[i] == 2 {
			nums[right], nums[i] = nums[i], nums[right]
			right--
		}
	}
}

func sortColors(nums []int) {
	i := 0
	left := 0
	right := len(nums) - 1
	for i <= right {
		if nums[i] == 0 {
			nums[i], nums[left] = nums[left], nums[i]
			left++
			i++
		} else if nums[i] == 2 {
			nums[i], nums[right] = nums[right], nums[i]
			right--
		} else {
			i++
		}
	}
}
