package three_sum

import (
	"sort"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestThreeSum(t *testing.T) {
	testCases := []struct {
		input    []int
		expected [][]int
	}{
		{
			[]int{-1, 0, 1, 2, -1, -4},
			[][]int{{-1, -1, 2}, {-1, 0, 1}},
		},
		{
			[]int{0, 1, 1},
			[][]int{},
		},
	}

	for _, tc := range testCases {
		actual := threeSum(tc.input)
		tr := cmp.Transformer("sort", func(slices [][]int) [][]int {
			for _, slice := range slices {
				sort.Ints(slice)
			}
			sort.Slice(slices, func(i, j int) bool {
				for x := range slices[i] {
					if slices[i][x] == slices[j][x] {
						continue
					}
					return slices[i][x] < slices[j][x]
				}
				return false
			})
			return slices
		})

		if !cmp.Equal(actual, tc.expected, tr) {
			t.Errorf("For input=%v, expected %v, but got %v", tc.input, tc.expected, actual)
		}

	}
}
