package three_sum

import "sort"

func threeSum(nums []int) [][]int {

	sort.Ints(nums)
	res := [][]int{}
	m := map[[3]int][]int{}
	for i := 0; i < len(nums); i++ {
		l := i + 1
		r := len(nums) - 1
		for l < r {
			if nums[i]+nums[l]+nums[r] == 0 {
				temp := [3]int{nums[i], nums[l], nums[r]}
				sort.Ints(temp[:])
				m[temp] = []int{nums[i], nums[l], nums[r]}
				l++
				r--
			} else if nums[i]+nums[l]+nums[r] > 0 {
				r--
			} else {
				l++
			}

		}
	}
	for _, v := range m {
		res = append(res, v)
	}
	return res
}
