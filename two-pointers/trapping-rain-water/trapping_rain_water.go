package trappingrainwater

// time complexity: O(n)
// space complexity: O(n)
func trap2(height []int) int {
	leftMax := make([]int, len(height))
	rightMax := make([]int, len(height))
	leftMax[0] = height[0]
	for i := 1; i < len(height); i++ {
		leftMax[i] = max(height[i], leftMax[i-1])
	}
	rightMax[len(rightMax)-1] = height[len(height)-1]
	for i := len(height) - 2; i >= 0; i-- {
		rightMax[i] = max(height[i], rightMax[i+1])
	}
	area := 0

	for i := 0; i < len(height); i++ {
		area += min(leftMax[i], rightMax[i]) - height[i]
	}
	return area
}

// time complexity: O(n)
// space complexity: O(1)
func trap(height []int) int {
	n := len(height)
	left, right := 0, n-1
	leftbar := height[0]
	rightbar := height[n-1]
	area := 0
	for left <= right {
		if leftbar < rightbar {
			if height[left] > leftbar {
				leftbar = height[left]
			} else {
				area += leftbar - height[left]
				left++
			}
		} else {
			if height[right] > rightbar {
				rightbar = height[right]
			} else {
				area += rightbar - height[right]
				right--
			}
		}
	}
	return area
}
