package remove_duplicates

func removeDuplicates(nums []int) int {
	left := 0
	for i := 0; i < len(nums)-1; i++ {
		if nums[i] != nums[i+1] {
			left++
			nums[left] = nums[i+1]
		}
	}
	return left + 1
}
