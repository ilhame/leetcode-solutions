package remove_duplicates

import "testing"

func TestRemoveDuplicates(t *testing.T) {
	testCases := []struct {
		input    []int
		expected int
	}{
		{
			[]int{1, 1, 2},
			2,
		},
		{
			[]int{0, 0, 1, 1, 1, 2, 2, 3, 3, 4},
			5,
		},
	}

	for _, tc := range testCases {
		actual := removeDuplicates(tc.input)
		if actual != tc.expected {
			t.Errorf("For input %v expected %d but got %d", tc.input, tc.expected, actual)
		}
	}
}
