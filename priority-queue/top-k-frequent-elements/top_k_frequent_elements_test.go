package top_k_frequent_elements

import (
	"reflect"
	"sort"
	"testing"
)

func TestTopKFrequent(t *testing.T) {
	tests := []struct {
		nums []int
		k    int
		want []int
	}{
		{[]int{1, 1, 1, 2, 2, 3}, 2, []int{1, 2}},
		{[]int{4, 1, -1, 2, -1, 2, 3}, 2, []int{-1, 2}},
		{[]int{5, 5, 5, 2, 2, 3, 3, 3, 3}, 2, []int{3, 5}},
		{[]int{1, 2, 3, 4, 5}, 5, []int{1, 2, 3, 4, 5}},
	}

	for _, test := range tests {
		t.Run("", func(t *testing.T) {
			got := topKFrequent(test.nums, test.k)

			// Since the order of output can be different, we sort the slices before comparing.
			sort.Ints(got)
			sort.Ints(test.want)

			if !reflect.DeepEqual(got, test.want) {
				t.Errorf("For nums %v and k %d, got %v, want %v", test.nums, test.k, got, test.want)
			}
		})
	}
}
