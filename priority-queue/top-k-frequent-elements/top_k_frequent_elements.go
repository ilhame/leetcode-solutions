package top_k_frequent_elements

import (
	"container/heap"
)

type element struct {
	Val   int
	Count int
}

type IntHeap []element

func (h IntHeap) Len() int {
	return len(h)
}
func (h IntHeap) Less(i, j int) bool {
	return h[i].Count > h[j].Count
}
func (h IntHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *IntHeap) Push(val any) {
	*h = append(*h, val.(element))
}

func (h *IntHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func topKFrequent(nums []int, k int) []int {
	h := &IntHeap{}
	heap.Init(h)
	m := map[int]int{}
	for i := 0; i < len(nums); i++ {
		m[nums[i]]++
	}
	for k, v := range m {
		heap.Push(h, element{Val: k, Count: v})
	}
	res := []int{}

	for i := 0; i < k; i++ {
		if val, ok := heap.Pop(h).(element); ok {
			res = append(res, val.Val)
		}
	}
	return res

}
