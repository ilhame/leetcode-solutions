package top_k_frequent_words

import "container/heap"

type Elem struct {
	value string
	count int
}

type MaxHeap []Elem

func (h *MaxHeap) Push(val any) {
	*h = append(*h, val.(Elem))
}

func (h *MaxHeap) Pop() any {
	vh := *h
	last := (vh)[len(vh)-1]
	*h = vh[:len(vh)-1]
	return last
}

func (h *MaxHeap) Len() int {
	return len(*h)
}

func (h *MaxHeap) Less(i, j int) bool {
	vh := *h
	if vh[i].count == vh[j].count {
		return vh[i].value < vh[j].value
	}
	return vh[i].count > vh[j].count
}

func (h *MaxHeap) Swap(i, j int) {
	vh := *h
	vh[i], vh[j] = vh[j], vh[i]
}

func topKFrequent(words []string, k int) []string {
	maxHeap := &MaxHeap{}
	heap.Init(maxHeap)
	seen := map[string]int{}
	for i := 0; i < len(words); i++ {
		seen[words[i]]++
	}
	for k, v := range seen {
		heap.Push(maxHeap, Elem{value: k, count: v})
	}
	res := []string{}
	for i := 0; i < k; i++ {
		val := heap.Pop(maxHeap)
		res = append(res, val.(Elem).value)
	}
	return res
}
