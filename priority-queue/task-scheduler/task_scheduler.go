package task_scheduler

type MaxHeap struct {
	arr []int
}

func (h *MaxHeap) Len() int {
	return len(h.arr)
}

func (h *MaxHeap) Push(elem int) {
	h.arr = append(h.arr, elem)
	i := len(h.arr) - 1
	p := h.Parent(i)
	for h.arr[i] > h.arr[p] {
		h.Swap(i, p)
		i = p
		p = h.Parent(i)
	}

}

func (h *MaxHeap) Pop() int {
	maxELem := h.arr[0]
	h.Swap(0, len(h.arr)-1)
	h.arr = h.arr[:len(h.arr)-1]
	i := 0
	l := h.Left(i)
	r := h.Right(i)
	childToCompare := 0
	for l < len(h.arr) {
		if l == len(h.arr)-1 {
			childToCompare = l
		} else {
			if h.arr[l] < h.arr[r] {
				childToCompare = r
			} else {
				childToCompare = l
			}
		}

		if h.arr[childToCompare] > h.arr[i] {
			h.Swap(i, childToCompare)
			i = childToCompare
			l = h.Left(i)
			r = h.Right(i)
		} else {
			break
		}
	}

	return maxELem

}

func (h *MaxHeap) Parent(i int) int {
	return (i - 1) / 2
}

func (h *MaxHeap) Left(i int) int {
	return 2*i + 1
}
func (h *MaxHeap) Right(i int) int {
	return 2*i + 2
}

func (h *MaxHeap) Swap(i, j int) {
	h.arr[i], h.arr[j] = h.arr[j], h.arr[i]
}

func leastInterval(tasks []byte, n int) int {
	heap := MaxHeap{}
	seen := map[byte]int{}
	for i := 0; i < len(tasks); i++ {
		seen[tasks[i]]++
	}
	for _, v := range seen {
		heap.Push(v)
	}

	timeUnit := 0
	queue := [][2]int{}

	for heap.Len() > 0 || len(queue) > 0 {
		timeUnit++
		if len(queue) > 0 {
			x := queue[0]
			if timeUnit >= x[1] {
				queue = queue[1:]
				heap.Push(x[0])
			}
		}
		if heap.Len() > 0 {
			task := heap.Pop()
			task--
			if task > 0 {
				queue = append(queue, [2]int{task, timeUnit + n + 1})
			}
		}

	}

	return timeUnit

}
