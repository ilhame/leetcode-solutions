package sender_with_largest_word_count

import (
	"container/heap"
	"strings"
)

type Elem struct {
	value string
	count int
}

type MaxHeap22 []Elem

func (h *MaxHeap22) Push(val any) {
	*h = append(*h, val.(Elem))
}

func (h *MaxHeap22) Pop() any {
	vh := *h
	last := (vh)[len(vh)-1]
	*h = vh[:len(vh)-1]
	return last
}

func (h *MaxHeap22) Len() int {
	return len(*h)
}

func (h *MaxHeap22) Less(i, j int) bool {
	vh := *h
	if vh[i].count == vh[j].count {
		return vh[i].value > vh[j].value
	}
	return vh[i].count > vh[j].count
}

func (h *MaxHeap22) Swap(i, j int) {
	vh := *h
	vh[i], vh[j] = vh[j], vh[i]
}

func largestWordCount(messages []string, senders []string) string {
	m := map[string]int{}
	for i, sender := range senders {
		messages := strings.Split(messages[i], " ")
		m[sender] += len(messages)
	}
	maxHeap := &MaxHeap22{}
	heap.Init(maxHeap)
	for k, v := range m {
		heap.Push(maxHeap, Elem{value: k, count: v})
	}
	val := heap.Pop(maxHeap)
	res := val.(Elem).value
	return res
}
