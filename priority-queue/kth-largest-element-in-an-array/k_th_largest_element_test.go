package kth_largest_element

import (
	"testing"
)

func TestFindKthLargest(t *testing.T) {
	tests := []struct {
		nums []int
		k    int
		want int
	}{
		{[]int{3, 2, 1, 5, 4}, 2, 4},
		{[]int{5, 4, 3, 2, 1}, 3, 3},
		{[]int{1, 2, 3, 4, 5}, 1, 5},
		{[]int{5, 5, 4, 3, 2, 1}, 4, 3},
		{[]int{1}, 1, 1},
		{[]int{-1, -2, -3, -4, -5}, 3, -3},
		{[]int{10, 7, 8, 6, 2, 1, 3}, 5, 3},
	}

	for _, test := range tests {
		t.Run("", func(t *testing.T) {
			got := findKthLargest(test.nums, test.k)
			if got != test.want {
				t.Errorf("For nums %v and k %d, got %d, want %d", test.nums, test.k, got, test.want)
			}
		})
	}
}
