package kth_largest_element

func findKthLargest(nums []int, k int) int {

	heap := &MaxHeap{}
	for _, num := range nums {
		heap.Insert(num)
	}
	res := 0
	for i := 0; i < k; i++ {
		res = heap.Delete()
	}
	return res
}

type MaxHeap struct {
	arr []int
}

func (h *MaxHeap) Insert(val int) {
	h.arr = append(h.arr, val)
	h.heapifyUp(len(h.arr) - 1)
}

func (h *MaxHeap) Delete() int {
	el := h.arr[0]
	h.Swap(0, len(h.arr)-1)
	h.arr = h.arr[:len(h.arr)-1]
	h.heapifyDown(0)
	return el
}

func (h *MaxHeap) heapifyDown(idx int) {
	childToCompare := 0
	left := Left(idx)
	right := Right(idx)
	for left <= len(h.arr)-1 {
		if left == len(h.arr)-1 || h.arr[left] > h.arr[right] {
			childToCompare = left
		} else {
			childToCompare = right
		}
		if h.arr[idx] < h.arr[childToCompare] {
			h.Swap(idx, childToCompare)
			idx = childToCompare
			left = Left(idx)
			right = Right(idx)
		} else {
			return
		}
	}

}

func Left(idx int) int {
	return 2*idx + 1
}

func Right(idx int) int {
	return 2*idx + 2
}

func Parent(idx int) int {
	return (idx - 1) / 2
}

func (h *MaxHeap) Swap(i, j int) {
	h.arr[i], h.arr[j] = h.arr[j], h.arr[i]
}
func (h *MaxHeap) heapifyUp(idx int) {
	for h.arr[idx] > h.arr[Parent(idx)] {
		h.Swap(idx, Parent(idx))
		idx = Parent(idx)
	}
}
