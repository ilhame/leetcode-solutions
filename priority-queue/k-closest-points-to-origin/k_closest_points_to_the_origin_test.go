package k_closest_points_to_origin

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func Test_KClosest(t *testing.T) {
	testCases := []struct {
		points   [][]int
		k        int
		expected [][]int
	}{
		{
			[][]int{{1, 3}, {-2, 2}},
			1,
			[][]int{{-2, 2}},
		},
		{
			[][]int{{3, 3}, {5, -1}, {-2, 4}},
			2,
			[][]int{{3, 3}, {-2, 4}},
		},
	}

	for _, tc := range testCases {
		got := kClosest(tc.points, tc.k)
		if !cmp.Equal(got, tc.expected) {
			t.Errorf("for input %v expected %v but got %v", tc.points, got, tc.expected)
		}
	}
}
