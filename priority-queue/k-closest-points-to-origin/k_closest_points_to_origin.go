package k_closest_points_to_origin

import "container/heap"

type MinHeap [][]int

func (h MinHeap) Len() int           { return len(h) }
func (h MinHeap) Less(i, j int) bool { return h[i][0] < h[j][0] }
func (h MinHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *MinHeap) Push(val any) {
	*h = append(*h, val.([]int))
}

func (h *MinHeap) Pop() any {
	last := (*h)[len(*h)-1]
	*h = (*h)[:len(*h)-1]
	return last
}

func kClosest(points [][]int, k int) [][]int {
	myheap := &MinHeap{}
	heap.Init(myheap)
	for _, point := range points {
		d := distance(point[0], point[1])
		heap.Push(myheap, []int{d, point[0], point[1]})
	}
	res := [][]int{}
	for k > 0 {
		tmp := heap.Pop(myheap).([]int)
		res = append(res, []int{tmp[1], tmp[2]})
		k--
	}
	return res
}

func distance(x, y int) int {
	return x*x + y*y
}
