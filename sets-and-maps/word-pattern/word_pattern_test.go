package word_pattern

import "testing"

func TestWordPattern(t *testing.T) {
	testCases := []struct {
		pattern  string
		s        string
		expected bool
	}{
		{
			"abba",
			"dog cat cat dog",
			true,
		},
		{
			"abba",
			"dog cat cat fish",
			false,
		},
		{
			"aaaa",
			"dog cat cat dog",
			false,
		},
		{
			"abba",
			"dog dog dog dog",
			false,
		},
		{
			"aaa",
			"aa aa aa aa",
			false,
		},
	}

	for _, tc := range testCases {
		res := wordPattern(tc.pattern, tc.s)
		if res != tc.expected {
			t.Errorf("For input=%v %v, expected %v, but got %v", tc.pattern, tc.s, tc.expected, res)

		}
	}
}
