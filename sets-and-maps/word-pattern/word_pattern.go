package word_pattern

import "strings"

// Time Complexity: O(m + n)
// Space Complexity: O(min(n, m))
func wordPattern(pattern string, s string) bool {
	words := strings.Split(s, " ")
	if len(words) != len(pattern) {
		return false
	}
	seen := map[byte]string{}
	seen2 := map[string]byte{}

	for i := 0; i < len(pattern); i++ {
		if val, ok := seen[pattern[i]]; ok {
			if val != words[i] {
				return false
			}
		}
		seen[pattern[i]] = words[i]
	}

	for i := 0; i < len(words); i++ {
		if val, ok := seen2[words[i]]; ok {
			if val != pattern[i] {
				return false
			}
		}
		seen2[words[i]] = pattern[i]
	}
	return true
}
