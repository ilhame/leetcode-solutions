package intersection_of_two_arrays

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestIntersection(t *testing.T) {
	testCases := []struct {
		nums1    []int
		nums2    []int
		expected []int
	}{
		{[]int{1, 2, 2, 1}, []int{2, 2}, []int{2}},
		{[]int{4, 9, 5}, []int{9, 4, 9, 8, 4}, []int{9, 4}},
		{[]int{1, 2, 3}, []int{4, 5, 6}, []int{}},
	}

	for _, tc := range testCases {
		result := intersection(tc.nums1, tc.nums2)
		if testEq(tc.nums1, tc.nums2) {
			t.Errorf("For nums1=%v, nums2=%v, expected %v, but got %v", tc.nums1, tc.nums2, tc.expected, result)
		}
	}
}

func testEq(nums1, nums2 []int) bool {
	equal := cmp.Equal(nums1, nums2, cmpopts.SortSlices(func(a, b int) bool { return a < b }))
	return equal
}
