package intersection_of_two_arrays

// Time Complexity: O(n + m)
// Space Complexity: O(n + m)
// where n is the length of nums1 and m is the length of nums2.
func intersection(nums1 []int, nums2 []int) []int {
	seen := map[int]bool{}
	for i := 0; i < len(nums1); i++ {
		seen[nums1[i]] = true
	}
	res := []int{}

	for i := 0; i < len(nums2); i++ {
		if seen[nums2[i]] {
			res = append(res, nums2[i])
			seen[nums2[i]] = false
		}
	}
	return res
}
