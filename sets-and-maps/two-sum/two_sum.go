package two_sum

// Time Complexity: o(n)
// Space Complexity: o(n)
func twoSum(nums []int, target int) []int {
	seen := map[int]int{}
	for i := 0; i < len(nums); i++ {
		if idx, ok := seen[target-nums[i]]; ok {
			return []int{idx, i}
		}
		seen[nums[i]] = i
	}
	return []int{}
}

// Brute Force
// Time Complexity: O(n^2)
// Space Complexity: O(1)
func twoSumAlt(nums []int, target int) []int {
	for i := 0; i < len(nums)-1; i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i]+nums[j] == target {
				return []int{i, j}
			}
		}
	}
	return []int{}
}
