package two_sum

import (
	"reflect"
	"testing"
)

func TestTwoSum(t *testing.T) {
	testCases := []struct {
		nums     []int
		target   int
		expected []int
	}{
		{[]int{2, 7, 11, 15}, 9, []int{0, 1}},
		{[]int{3, 2, 4}, 6, []int{1, 2}},
		{[]int{3, 3}, 6, []int{0, 1}},
	}

	for _, testCase := range testCases {
		result := twoSum(testCase.nums, testCase.target)
		if !reflect.DeepEqual(result, testCase.expected) {
			t.Errorf("For nums=%v, target=%d, expected=%v, but got=%v", testCase.nums, testCase.target, testCase.expected, result)
		}
	}
}
