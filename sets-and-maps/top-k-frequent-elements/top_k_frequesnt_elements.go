package top_k_frequent_elements

import "sort"

// Time Complexity: O(n + m * log(m))
// Space Complexity: O(n + m)
// where m is the number of unique elements in nums.
// where n is the length of the nums array.
func topKFrequent(nums []int, k int) []int {
	seen := map[int]int{}

	for i := 0; i < len(nums); i++ {
		seen[nums[i]]++
	}
	res := []int{}
	for k := range seen {
		res = append(res, k)
	}
	sort.Slice(res, func(i, j int) bool {
		return seen[res[i]] > seen[res[j]]
	})

	return res[:k]

}
