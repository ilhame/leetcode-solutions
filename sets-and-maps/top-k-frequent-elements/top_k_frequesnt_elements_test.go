package top_k_frequent_elements

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestTopKFrequent(t *testing.T) {
	testCases := []struct {
		nums     []int
		k        int
		expected []int
	}{
		{
			[]int{1, 1, 1, 2, 2, 3},
			2,
			[]int{1, 2},
		},
		{
			[]int{4, 1, -1, 2, -1, 2, 3},
			2,
			[]int{-1, 2},
		},
		{
			[]int{5, 5, 1, 1, 1, 2, 2, 3},
			3,
			[]int{1, 5, 2},
		},
	}

	for _, tc := range testCases {
		result := topKFrequent(tc.nums, tc.k)
		if !testEq(result, tc.expected) {
			t.Errorf("For nums=%v, k=%d, expected %v, but got %v", tc.nums, tc.k, tc.expected, result)
		}
	}
}

func testEq(nums1, nums2 []int) bool {
	equal := cmp.Equal(nums1, nums2, cmpopts.SortSlices(func(a, b int) bool { return a < b }))
	return equal
}
