package subarraysumequalsk

// subarraySum returns count of subarrays that sum up to k
func subarraySum(nums []int, k int) int {
	seen := map[int]int{}
	seen[0] = 1
	count := 0
	sum := 0
	for i := 0; i < len(nums); i++ {
		sum += nums[i]
		seen[sum]++
		if val, ok := seen[sum-k]; ok {
			count += val
		}
	}
	return count
}
