package group_anagrams

import (
	"sort"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGroupAnagrams(t *testing.T) {
	testCases := []struct {
		input    []string
		expected [][]string
	}{
		{
			[]string{"eat", "tea", "tan", "ate", "nat", "bat"},
			[][]string{{"bat"}, {"nat", "tan"}, {"ate", "eat", "tea"}},
		},
		{
			[]string{"listen", "silent", "car", "arc", "god", "dog"},
			[][]string{{"listen", "silent"}, {"car", "arc"}, {"god", "dog"}},
		},
		{
			[]string{"hello", "world", "abcd", "dlrow", "dbca", "bye"},
			[][]string{{"hello"}, {"world", "dlrow"}, {"abcd", "dbca"}, {"bye"}},
		},
		{
			[]string{},
			[][]string{},
		},
		{
			[]string{"a"},
			[][]string{{"a"}},
		},
	}

	for _, tc := range testCases {
		result := groupAnagrams(tc.input)
		groupComparer2 := cmp.Transformer("SortGroups", func(groups [][]string) [][]string {
			for _, group := range groups {
				sort.Strings(group)
			}
			sort.Slice(groups, func(i, j int) bool {
				for x := range groups[i] {
					if groups[i][x] == groups[j][x] {
						continue
					}
					return groups[i][x] < groups[j][x]
				}
				return false
			})
			return groups
		})

		if !cmp.Equal(result, tc.expected, groupComparer2) {
			t.Errorf("For input=%v, expected %v, but got %v", tc.input, tc.expected, result)
		}
	}
}
