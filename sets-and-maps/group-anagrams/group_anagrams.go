package group_anagrams

import "sort"

func groupAnagrams(strs []string) [][]string {
	m := map[string][]string{}
	res := [][]string{}
	for _, str := range strs {
		m[sortStr(str)] = append(m[sortStr(str)], str)
	}
	for _, v := range m {
		res = append(res, v)
	}
	return res
}
func sortStr(s string) string {
	runes := []rune(s)
	sort.Slice(runes, func(i, j int) bool {
		return runes[i] < runes[j]
	})
	return string(runes)
}
