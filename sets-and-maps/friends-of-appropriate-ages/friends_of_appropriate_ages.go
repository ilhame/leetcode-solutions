package friendsofappropriateages

func numFriendRequests(ages []int) int {
	count := 0
	m1 := map[int]int{}
	m2 := map[int]int{}

	for _, age := range ages {
		m1[age]++
	}
	for _, age := range ages {
		m2[age]++
	}

	for k1, v1 := range m1 {
		for k2, v2 := range m2 {
			if k2 > 100 && k1 < 100 || k2 > k1 || k2 <= k1/2+7 {
				continue
			}
			count += v1 * v2
			if k1 == k2 {
				count -= v1
			}
		}
	}
	return count
}
