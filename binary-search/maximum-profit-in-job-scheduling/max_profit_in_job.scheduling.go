package maximumprofitinjobscheduling

import "sort"

type Job struct {
	StartTime int
	EndTime   int
	Profit    int
}

func jobSchedulingImproved(startTime []int, endTime []int, profit []int) int {
	jobs := make([]Job, len(startTime))
	for i := 0; i < len(jobs); i++ {
		jobs[i] = Job{
			StartTime: startTime[i],
			EndTime:   endTime[i],
			Profit:    profit[i],
		}
	}

	sort.Slice(jobs, func(i, j int) bool {
		return jobs[i].EndTime < jobs[j].EndTime
	})
	profits := make([]int, len(jobs))
	profits[0] = jobs[0].Profit

	for i := 1; i < len(jobs); i++ {
		low, high := 0, i-1
		index := -1

		for low <= high {
			mid := low + (high-low)/2
			if jobs[mid].EndTime <= jobs[i].StartTime {
				index = mid
				low = mid + 1
			} else {
				high = mid - 1
			}
		}

		if index != -1 {
			profits[i] = max(profits[i-1], profits[index]+jobs[i].Profit)
		} else {
			profits[i] = max(profits[i-1], jobs[i].Profit)
		}
	}
	return profits[len(profits)-1]
}

func jobScheduling(startTime []int, endTime []int, profit []int) int {
	jobs := make([]Job, len(startTime))
	for i := 0; i < len(jobs); i++ {
		jobs[i] = Job{
			StartTime: startTime[i],
			EndTime:   endTime[i],
			Profit:    profit[i],
		}
	}

	sort.Slice(jobs, func(i, j int) bool {
		return jobs[i].EndTime < jobs[j].EndTime
	})
	profits := make([]int, len(jobs))
	for i := 0; i < len(jobs); i++ {
		profits[i] = jobs[i].Profit
	}

	for i := 1; i < len(jobs); i++ {

		for j := 0; j < i; j++ {
			if jobs[i].StartTime >= jobs[j].EndTime {
				profits[i] = max(profits[i], jobs[i].Profit+profits[j])
			}
		}

	}
	max := profits[0]
	for _, profit := range profits {
		if profit > max {
			max = profit
		}
	}

	return max

}
