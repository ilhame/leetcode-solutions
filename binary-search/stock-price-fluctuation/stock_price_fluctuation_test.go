package stockpricefluctuation

import (
	"testing"
)

type Operation struct {
	Method    string
	Timestamp int
	Price     int
}

func TestStockPrice(t *testing.T) {
	testCases := []struct {
		name       string
		operations []Operation
		expected   []int
	}{
		{
			name: "Basic operations",
			operations: []Operation{
				{"update", 1, 10}, {"update", 2, 5}, {"current", 0, 0},
				{"maximum", 0, 0}, {"update", 1, 3}, {"maximum", 0, 0},
				{"update", 4, 2}, {"minimum", 0, 0},
			},
			expected: []int{0, 0, 5, 10, 0, 5, 0, 2},
		},
		// Add more test cases as needed
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			c := Constructor33()
			for i, op := range tc.operations {
				switch op.Method {
				case "update":
					c.Update(op.Timestamp, op.Price)
				case "current":
					actual := c.Current()
					if actual != tc.expected[i] {
						t.Errorf("Current() = %d, expected %d", actual, tc.expected[i])
					}
				case "maximum":
					actual := c.Maximum()
					if actual != tc.expected[i] {
						t.Errorf("Maximum() = %d, expected %d", actual, tc.expected[i])
					}
				case "minimum":
					actual := c.Minimum()
					if actual != tc.expected[i] {
						t.Errorf("Minimum() = %d, expected %d", actual, tc.expected[i])
					}
				}
			}
		})
	}
}
