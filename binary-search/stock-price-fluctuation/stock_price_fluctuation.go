package stockpricefluctuation

type StockPrice struct {
	prices       []int
	curTimestamp int
	curVal       int
	data         map[int]int
}

func Constructor33() StockPrice {
	return StockPrice{
		data: make(map[int]int),
	}
}

// [1,2,4,5] 6
func (this *StockPrice) Update(timestamp int, price int) {
	if timestamp >= this.curTimestamp {
		this.curTimestamp = timestamp
		this.curVal = price
	}
	if oldPrice, ok := this.data[timestamp]; ok {
		l, r := 0, len(this.prices)
		for l <= r {
			mid := l + (r-l)/2
			if this.prices[mid] == oldPrice {
				copy(this.prices[mid:], this.prices[mid+1:])
				this.prices = this.prices[:len(this.prices)-1]
				return
			} else if this.prices[mid] < oldPrice {
				l = mid + 1
			} else {
				r = mid - 1
			}
		}

	}
	this.data[timestamp] = price
	l, r := 0, len(this.prices)-1
	for l <= r {
		mid := l + (r-l)/2
		if this.prices[mid] < price {
			l = mid + 1
		} else {
			r = mid - 1
		}

	}
	this.prices = append(this.prices, 0)
	copy(this.prices[l+1:], this.prices[l:])
	this.prices[l] = price
}

func (this *StockPrice) Current() int {
	return this.curVal
}
func (this *StockPrice) Maximum() int {
	return this.prices[len(this.prices)-1]
}

func (this *StockPrice) Minimum() int {
	return this.prices[0]

}
