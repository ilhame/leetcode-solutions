package search_in_a_rotated_sorted_array

func search(nums []int, target int) int {

	l, r := 0, len(nums)-1

	for l <= r {
		mid := l + (r-l)/2
		if nums[mid] == target {
			return mid
		}
		if nums[l] < nums[mid] {
			if nums[l] <= target && target <= nums[mid] {
				r = mid
			} else {
				l = mid + 1
			}
		} else {
			if nums[mid] <= target && target <= nums[r] {
				l = mid
			} else {
				r = mid - 1
			}
		}
	}
	return -1

}
