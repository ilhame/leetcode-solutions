package search_in_a_rotated_sorted_array

import "testing"

func TestSearch(t *testing.T) {
	testCases := []struct {
		nums     []int
		target   int
		expected int
	}{
		{
			[]int{7, 8, 9, 1, 2, 3, 4, 5, 6},
			4,
			6,
		},
		{
			[]int{3, 4, 5, 6, 0, 1, 2},
			2,
			6,
		},
	}

	for _, tc := range testCases {
		actual := search(tc.nums, tc.target)
		if actual != tc.expected {
			t.Errorf("For input %v expected %d but got %d", tc.nums, tc.expected, actual)
		}
	}
}
