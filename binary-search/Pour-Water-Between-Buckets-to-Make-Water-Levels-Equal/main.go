package pourwater

func equalizeWater(buckets []int, loss int) float64 {
	var left float64
	var right float64 = float64(max(buckets))

	for right-left > 1e-5 {
		mid := (left + right) / 2
		if canEqualize(buckets, mid, loss) {
			left = mid
		} else {
			right = mid
		}
	}
	return left
}

func max(arr []int) int {
	maxVal := arr[0]
	for _, v := range arr {
		if v > maxVal {
			maxVal = v
		}
	}
	return maxVal
}
func canEqualize(buckets []int, target float64, loss int) bool {
	var a, b float64

	for _, bucket := range buckets {
		if float64(bucket) > target {
			a += float64(bucket) - target
		} else {
			b += (target - float64(bucket)) * 100 / float64(100-loss)

		}
	}
	return a >= b
}
