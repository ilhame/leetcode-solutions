package find_the_duplicate

// Time complexity O(nlogn)
// Space complecity O(1)
func findDuplicate(nums []int) int {
	low, high := 0, len(nums)-1
	duplicate := 0

	for low <= high {
		mid := low + (high-low)/2
		count := 0
		for i := 0; i < len(nums); i++ {
			if nums[i] <= mid {
				count++
			}
		}
		if count > mid {
			duplicate = mid
			high = mid - 1
		} else {
			low = mid + 1
		}
	}
	return duplicate
}

func findDuplicateAlt2(nums []int) int {
	slow := nums[0]
	fast := nums[0]

	for {
		if slow == fast {
			break
		}
		slow = nums[slow]
		fast = nums[nums[fast]]
	}

	newSlow := nums[0]

	for {
		slow = nums[slow]
		newSlow = nums[newSlow]
	}
	return nums[slow]
}

// negative marking
// [3, 2, 1, 2, 4, 5, 3]
func findDuplicateAlt(nums []int) int {
	for i := 0; i < len(nums); i++ {
		idx := Abs(nums[i])
		if nums[idx] < 0 {
			return idx
			break
		}
		nums[idx] *= -1

	}

	for i := 0; i < len(nums); i++ {
		nums[i] = Abs(nums[i])
	}
	return -1
}

func Abs(num int) int {
	if num < 0 {
		return -num
	}
	return num
}
