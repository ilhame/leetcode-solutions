package valid_perfect_square

import "testing"

func TestIsPerfectSquare(t *testing.T) {
	testCases := []struct {
		input    int
		expected bool
	}{
		{
			16,
			true,
		},
		{
			14,
			false,
		},
		{
			1,
			true,
		},
	}

	for _, tc := range testCases {
		actual := isPerfectSquare(tc.input)
		if actual != tc.expected {
			t.Errorf("For Input %v expected %v but got %v", tc.input, tc.expected, actual)
		}
	}
}
