package sqrt

import "testing"

func TestMySqrt(t *testing.T) {
	testCases := []struct {
		x        int
		expected int
	}{
		{
			16,
			4,
		},
		{
			1,
			1,
		},
		{
			8,
			2,
		},
	}

	for _, tc := range testCases {
		actual := mySqrt(tc.x)
		if actual != tc.expected {
			t.Errorf("For input %v expected %d but got %d", tc.x, tc.expected, actual)
		}
	}
}
