package merge_k_sorted_lists

import (
	"fmt"
	"testing"
)

func TestMergeKLists(t *testing.T) {
	tests := []struct {
		input    []*ListNode
		expected *ListNode
	}{
		{
			input: []*ListNode{
				createList([]int{1, 4, 5}),
				createList([]int{1, 3, 4}),
				createList([]int{2, 6}),
			},
			expected: createList([]int{1, 1, 2, 3, 4, 4, 5, 6}),
		},
		{
			input:    nil,
			expected: nil,
		},
	}

	for _, test := range tests {
		result := mergeKLists(test.input)
		println(listToString(result))
		if !isListEqual(result, test.expected) {
			t.Errorf("For input %v, expected %v, but got %v", test.input, listToString(test.expected), listToString(result))
		}
	}
}

func createList(nums []int) *ListNode {
	dummy := &ListNode{}
	res := dummy
	for _, num := range nums {
		res.Next = &ListNode{Val: num}
		res = res.Next
	}
	return dummy.Next
}

func listToString(list *ListNode) string {
	res := []int{}
	cur := list
	for cur != nil {
		res = append(res, cur.Val)
		cur = cur.Next
	}
	return fmt.Sprintf("%v", res)
}

func isListEqual(list1, list2 *ListNode) bool {
	for list1 != nil && list2 != nil {
		if list1.Val != list2.Val {
			return false
		}
		list1 = list1.Next
		list2 = list2.Next
	}
	return list1 == nil && list2 == nil
}

func TestMerge2Lists(t *testing.T) {
	tests := []struct {
		list1    *ListNode
		list2    *ListNode
		expected *ListNode
	}{
		{
			list1:    createList([]int{1, 4, 5}),
			list2:    createList([]int{1, 3, 4}),
			expected: createList([]int{1, 1, 3, 4, 4, 5}),
		},
		{
			list1:    nil,
			list2:    createList([]int{1, 2, 3}),
			expected: createList([]int{1, 2, 3}),
		},
		{
			list1:    createList([]int{1, 2, 3}),
			list2:    nil,
			expected: createList([]int{1, 2, 3}),
		},
	}

	for _, test := range tests {
		result := merge2Lists(test.list1, test.list2)
		if !isListEqual(result, test.expected) {
			t.Errorf("For input list1: %v, list2: %v, expected %v, but got %v", listToString(test.list1), listToString(test.list2), listToString(test.expected), listToString(result))
		}
	}
}
