package merge_k_sorted_lists

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeKLists(lists []*ListNode) *ListNode {
	if lists == nil || len(lists) == 0 {
		return nil
	}
	for len(lists) > 1 {
		l1 := lists[0]
		l2 := lists[1]
		lists = lists[2:]
		merged := merge2Lists(l1, l2)
		lists = append(lists, merged)
	}
	return lists[0]
}

func merge2Lists(list1, list2 *ListNode) *ListNode {
	res := &ListNode{}
	dummy := res
	for list1 != nil && list2 != nil {
		if list1.Val < list2.Val {
			dummy.Next = list1
			list1 = list1.Next
		} else {
			dummy.Next = list2
			list2 = list2.Next
		}
		dummy = dummy.Next
	}
	for list1 != nil {
		dummy.Next = list1
		dummy = dummy.Next
		list1 = list1.Next
	}
	for list2 != nil {
		dummy.Next = list2
		dummy = dummy.Next
		list2 = list2.Next
	}
	return res.Next
}
