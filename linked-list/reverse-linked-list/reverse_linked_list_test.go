package reverse_linked_list

import "testing"

func TestReverseLinkedList(t *testing.T) {
	testCases := []struct {
		input    []int
		expected []int
	}{

		{
			input:    []int{1, 2, 3, 4},
			expected: []int{4, 3, 2, 1},
		},
		{
			input:    []int{5, 6, 7, 8, 9},
			expected: []int{9, 8, 7, 6, 5},
		},
		{
			input:    []int{10},
			expected: []int{10},
		},
		{
			input:    []int{},
			expected: []int{},
		},
	}

	for _, tc := range testCases {

		var head *ListNode
		var prev *ListNode

		for i := 0; i < len(tc.input); i++ {
			node := &ListNode{Val: tc.input[i]}
			if prev == nil {
				head = node
			} else {
				prev.Next = node
			}
			prev = node
		}
		reversed := reverseList(head)

		for _, el := range tc.expected {
			if reversed.Val != el {
				t.Errorf("For Input %v expected %v but got %v", tc.input, el, reversed.Val)
			}
			reversed = reversed.Next
		}
	}
}
