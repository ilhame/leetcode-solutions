package reverselinkedlist2

type ListNode struct {
	Val  int
	Next *ListNode
}

func reverseBetween(head *ListNode, left int, right int) *ListNode {
	cur := head
	dummy := &ListNode{0, head}
	leftPrev, cur := dummy, head
	for i := 0; i < left-1; i++ {
		leftPrev, cur = cur, cur.Next
	}
	var prev *ListNode
	for i := 0; i < right-left+1; i++ {
		next := cur.Next
		cur.Next = prev
		prev = cur
		cur = next
	}
	leftPrev.Next.Next = cur
	leftPrev.Next = prev

	return dummy.Next
}
