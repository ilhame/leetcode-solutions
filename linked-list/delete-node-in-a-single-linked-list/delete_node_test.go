package delete_node

import (
	"testing"
)

func TestDeleteNode(t *testing.T) {
	testCases := []struct {
		input    []int
		deleteAt int
		expected []int
	}{
		{
			input:    []int{1, 2, 3, 4},
			deleteAt: 1,
			expected: []int{1, 3, 4},
		},
		{
			input:    []int{5, 6, 7, 8, 9},
			deleteAt: 3,
			expected: []int{5, 6, 7, 9},
		},
		{
			input:    []int{10, 11},
			deleteAt: 0,
			expected: []int{11},
		},
	}

	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			// Build the linked list
			var head *ListNode
			var prev *ListNode
			for _, val := range tc.input {
				node := &ListNode{Val: val}
				if prev == nil {
					head = node
				} else {
					prev.Next = node
				}
				prev = node
			}

			// Find the node to be deleted
			nodeToDelete := head
			for i := 0; i < tc.deleteAt; i++ {
				nodeToDelete = nodeToDelete.Next
			}
			// Call the deleteNode function
			deleteNode(nodeToDelete)

			// Check if the linked list is as expected
			current := head
			for _, val := range tc.expected {
				if current.Val != val {
					t.Errorf("Expected value %d, but got %d", val, current.Val)
				}
				current = current.Next
			}
		})
	}
}
