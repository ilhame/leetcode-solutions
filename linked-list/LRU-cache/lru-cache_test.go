package lrucache

import "testing"

func TestLRUCache(t *testing.T) {
	tests := []struct {
		name       string
		capacity   int
		operations []struct {
			method string
			args   []int
			want   int
		}
	}{
		{
			name:     "Test LRU cache",
			capacity: 2,
			operations: []struct {
				method string
				args   []int
				want   int
			}{
				{method: "Put", args: []int{1, 1}, want: -1},
				{method: "Put", args: []int{2, 2}, want: -1},
				{method: "Get", args: []int{1}, want: 1},
				{method: "Put", args: []int{3, 3}, want: -1},
				{method: "Get", args: []int{2}, want: -1},
				{method: "Put", args: []int{4, 4}, want: -1},
				{method: "Get", args: []int{1}, want: -1},
				{method: "Get", args: []int{3}, want: 3},
				{method: "Get", args: []int{4}, want: 4},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			lruCache := Constructor(test.capacity)
			for _, op := range test.operations {
				switch op.method {
				case "Get":
					got := lruCache.Get(op.args[0])
					if got != op.want {
						t.Errorf("Get(%d) = %d; want %d", op.args[0], got, op.want)
					}
				case "Put":
					lruCache.Put(op.args[0], op.args[1])
				default:
					t.Errorf("Unknown method: %s", op.method)

				}
			}
		})
	}
}

//[[2],[1,1],[2,2],[1],[3,3],[2],[4,4],[1],[3],[4]]
//[null,null,null,1,null,-1,null,-1,3,4]
