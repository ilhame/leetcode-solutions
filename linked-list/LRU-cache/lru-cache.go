package lrucache

import "container/list"

type LRUCache struct {
	cache    map[int]*list.Element
	capacity int
	order    *list.List
}

type CacheEntry struct {
	key   int
	value int
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		capacity: capacity,
		cache:    make(map[int]*list.Element, 0),
		order:    list.New(),
	}
}

func (this *LRUCache) Get(key int) int {
	if elem, ok := this.cache[key]; ok {
		this.order.MoveToFront(elem)
		return elem.Value.(*CacheEntry).value
	} else {
		return -1
	}
}

func (this *LRUCache) Put(key int, value int) {
	if elem, ok := this.cache[key]; ok {
		elem.Value.(*CacheEntry).value = value
		this.order.MoveToFront(elem)
	} else {
		cacheEntry := &CacheEntry{key: key, value: value}
		elem := this.order.PushFront(cacheEntry)
		this.cache[key] = elem
	}

	if len(this.cache) > this.capacity {
		lastElem := this.order.Back()
		delete(this.cache, lastElem.Value.(*CacheEntry).key)
		this.order.Remove(lastElem)
	}

}
