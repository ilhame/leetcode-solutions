package sortlist

type ListNode struct {
	Val  int
	Next *ListNode
}

func sortList(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	left := head
	right := getMid(head)
	temp := right.Next
	right.Next = nil
	right = temp

	left = sortList(left)
	right = sortList(right)
	return merge(left, right)

}

func merge(left, right *ListNode) *ListNode {
	dummy := &ListNode{}
	list := dummy

	for left != nil && right != nil {
		if left.Val < right.Val {
			list.Next = left
			left = left.Next
		} else {
			list.Next = right
			right = right.Next
		}
		list = list.Next
	}
	if left != nil {
		list.Next = left
	}
	if right != nil {
		list.Next = right
	}
	return dummy.Next
}

func getMid(head *ListNode) *ListNode {
	slow, fast := head, head.Next
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	return slow
}
