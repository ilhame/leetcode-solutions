package flood_fill

import (
	"reflect"
	"testing"
)

func TestFloodFill(t *testing.T) {
	testCases := []struct {
		name   string
		image  [][]int
		sc     int
		sr     int
		color  int
		output [][]int
	}{
		{
			name:   "testcase 1",
			image:  [][]int{{1, 1, 1}, {1, 1, 0}, {1, 0, 1}},
			sr:     1,
			sc:     1,
			color:  2,
			output: [][]int{{2, 2, 2}, {2, 2, 0}, {2, 0, 1}},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			actual := floodFill(tc.image, tc.sr, tc.sc, tc.color)
			if !testEq(actual, tc.output) {
				t.Errorf("for %s expected %v but got %v", tc.name, tc.output, actual)
			}
		})
	}
}

func testEq(list1, list2 [][]int) bool {
	if len(list1) != len(list2) {
		return false
	}

	for i := 0; i < len(list1); i++ {
		if !reflect.DeepEqual(list1[i], list2[i]) {
			return false
		}
	}

	return true
}
