package flood_fill

func floodFill(image [][]int, sr int, sc int, color int) [][]int {
	m := len(image)
	n := len(image[0])
	for i := sr; i < m; i++ {
		for j := sc; j < n; j++ {
			if image[sr][sc] != color {
				//image[sr][sc] = color
				dfs(image, sr, sc, color, image[sr][sc])
			}
		}
	}
	return image

}

func dfs(image [][]int, sr int, sc int, color int, source int) {
	m := len(image)
	n := len(image[0])
	if sr < 0 || sr >= m {
		return
	}
	if sc < 0 || sc >= n {
		return
	}
	if image[sr][sc] == color {
		return
	}
	if image[sr][sc] != source {
		return
	}
	image[sr][sc] = color
	dfs(image, sr+1, sc, color, source)
	dfs(image, sr-1, sc, color, source)
	dfs(image, sr, sc-1, color, source)
	dfs(image, sr, sc+1, color, source)
}
