package number_of_islands

func numIslands(grid [][]byte) int {
	if len(grid) == 0 {
		return 0
	}
	number_of_islands := 0
	rows := len(grid)
	cols := len(grid[0])

	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			if grid[i][j] == '1' {
				dfs(grid, i, j, rows, cols)
				number_of_islands += 1
			}

		}

	}
	return number_of_islands

}

func dfs(grid [][]byte, i, j int, rows, cols int) {
	m, n := len(grid), len(grid[0])
	if i < 0 || i >= m || j < 0 || j >= n || grid[i][j] != '1' {
		return
	}
	grid[i][j] = '2'
	dfs(grid, i+1, j, cols, rows)
	dfs(grid, i-1, j, cols, rows)
	dfs(grid, i, j+1, cols, rows)
	dfs(grid, i, j-1, cols, rows)

}
