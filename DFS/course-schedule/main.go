package main

import "fmt"

func main() {
	prerequisites := [][]int{{1, 0}}
	fmt.Println(canFinish(2, prerequisites))
}

// [[1,0],[0,1]]
func canFinish(numCourses int, prerequisites [][]int) bool {
	graph := map[int][]int{}

	for _, p := range prerequisites {
		graph[p[0]] = append(graph[p[0]], p[1])
	}
	visited := map[int]bool{}
	for i := 0; i < numCourses; i++ {
		if hasCycle(i, graph, visited) {
			return false
		}
	}
	return true
}

func hasCycle(course int, graph map[int][]int, visited map[int]bool) bool {
	if visited[course] {
		return true
	}

	if len(graph[course]) == 0 {
		return false
	}
	visited[course] = true

	for _, neighboor := range graph[course] {
		if hasCycle(neighboor, graph, visited) {
			return true
		}
	}
	visited[course] = false
	//graph[course] = []int{}
	return false
}
