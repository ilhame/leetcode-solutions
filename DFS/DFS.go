package dfs

type Graph struct {
	Vertices map[int][]int
}

func (g *Graph) AddEdge(from, to int) {
	g.Vertices[from] = append(g.Vertices[from], to)

}

func (g *Graph) DFS(node int, seen map[int]bool) {
	seen[node] = true
	neighboors := g.Vertices[node]
	for _, n := range neighboors {
		if !seen[n] {
			g.DFS(n, seen)
		}
	}
}

func main() {
	graph := &Graph{Vertices: make(map[int][]int)}
	graph.AddEdge(0, 1)
	graph.AddEdge(0, 2)
	graph.AddEdge(1, 2)
	graph.AddEdge(2, 0)
	graph.AddEdge(2, 3)
	graph.AddEdge(3, 3)
	visited := map[int]bool{}
	graph.DFS(0, visited)
}
