package keys_and_rooms

func canVisitAllRooms(rooms [][]int) bool {
	visited := map[int]bool{}
	dfs(0, visited, rooms)
	return len(visited) == len(rooms)
}

func dfs(room int, visited map[int]bool, rooms [][]int) {
	visited[room] = true
	for _, n := range rooms[room] {
		if !visited[n] {
			dfs(n, visited, rooms)
		}
	}
}
