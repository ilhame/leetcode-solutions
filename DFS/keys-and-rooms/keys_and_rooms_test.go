package keys_and_rooms

import "testing"

func TestCanVisitAllRooms(t *testing.T) {
	testCases := []struct {
		rooms    [][]int
		expected bool
	}{
		{
			[][]int{{1}, {2}, {3}, {}},
			true,
		},
		{
			[][]int{{1, 3}, {3, 0, 1}, {2}, {0}},
			false,
		},
	}
	for _, tc := range testCases {
		actual := canVisitAllRooms(tc.rooms)
		if actual != tc.expected {
			t.Errorf("for input %v ecpected %v but got %v ", tc.rooms, actual, tc.expected)
		}
	}
}
