package number_of_provinces

func findCircleNum(isConnected [][]int) int {
	visited := map[int]bool{}
	count := 0
	for i, _ := range isConnected {
		if !visited[i] {
			count++
			dfs(i, isConnected, visited)

		}
	}
	return count
}

func dfs(city int, isConnected [][]int, visited map[int]bool) {
	visited[city] = true
	for _, n := range isConnected[city] {
		if !visited[n] {
			if isConnected[city][n] == 1 {
				dfs(n, isConnected, visited)
			}
		}
	}
}
