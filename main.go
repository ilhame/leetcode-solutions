package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"
)

func main2() {
	timeout := 3 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	for {
		select {
		case <-time.After(1 * time.Second):
			time.Sleep(5 * time.Millisecond)
			fmt.Println("waited for 1 second")
		case <-time.After(2 * time.Second):
			fmt.Println("waited for 2 second")
			cancel()
		case <-time.After(3 * time.Second):
			fmt.Println("waited for 3 second")
		case <-ctx.Done():
			fmt.Println(ctx.Err())
			return

		}

	}
}

type RandomizedSet struct {
	nums []int
	set  map[int]int
}

func Constructor() RandomizedSet {
	return RandomizedSet{
		set: make(map[int]int),
	}
}

func (this *RandomizedSet) Insert(val int) bool {
	_, ok := this.set[val]
	if ok {
		return false
	}
	n := len(this.nums)
	this.set[val] = n
	this.nums = append(this.nums, val)
	return true
}

func (this *RandomizedSet) Remove(val int) bool {
	idx, ok := this.set[val]
	if !ok {
		return false
	}
	lastIdx := len(this.nums) - 1

	this.set[this.nums[lastIdx]] = idx
	this.nums[idx], this.nums[lastIdx] = this.nums[lastIdx], this.nums[idx]
	this.nums = this.nums[:lastIdx-1]
	delete(this.set, val)
	return true
}

func (this *RandomizedSet) GetRandom() int {
	r := rand.Intn(len(this.nums))
	return this.nums[r]
}

func main() {
	c := Constructor()
	c.Insert(1)
	c.Remove(2)
	c.Insert(2)
	c.GetRandom()
	c.Remove(1)
	c.Insert(2)
	c.GetRandom()

}
